package alexanderwojtowicz.documentClassifier;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.LinkedList;
import java.util.Set;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Description of <code>Train</code>
 * 
 * @since 1.0
 * @author tshumake
 */
public class Trainer {
/* ---------------------------------------------------------- MAIN ----------------------------------------------------------- */
	public static void main(String[] args) throws IOException {

    	Library library = new Library();
    	Options options = new Options();
    	HashMap<String, String> classOptions = new HashMap<String, String>();
    	classOptions = populateMap();
    	Option help = new Option("h", "help", false, "Print the help information.");   		
 
        options.addOption(help);
        options.addOption(OptionBuilder.withArgName("file").withDescription("File you want to train with.").hasArg().create("ClassifyFile"));
        options.addOption(OptionBuilder.withArgName("class").withDescription("The classification of a file.").hasArg().create("ClassifyType"));
        
        CommandLineParser parser = new BasicParser();
        try {
            // parse the command line arguments
            CommandLine cmd = parser.parse( options, args);
            
            String file_value = cmd.getOptionValue("ClassifyFile");
            String classify_value = cmd.getOptionValue("ClassifyType");

            if(cmd.hasOption("help") || args.length == 0 || file_value == null || classify_value == null) {
            	HelpFormatter formatter = new HelpFormatter();
            	formatter.printHelp("Train", options, true);
            }
            else {
            	// If classify_value is not valid...
            	if(!classOptions.containsKey(classify_value.toLowerCase())) {
            		System.out.println("*** ERROR: '" + classify_value + "' is not an option!");
            		printOptions();
            	}
            	// classify_value is valid, so train the system...
            	else {
            		//library.display();
            		System.out.println("Attempting to load file: " + new File(file_value).getCanonicalFile().toString());
            		Document toTrain = new Document(new File(file_value), classOptions.get(classify_value.toLowerCase()));
            		// The document is valid...
            		if(toTrain.isValid()) {
            			System.out.println("Successfully loaded: " + toTrain.getPath().toString());
            			toTrain = pruneDocSig(toTrain, library);
            			library = train(toTrain, library);
            			System.out.println("Done training...");
            			finish(library, new LinkedList<String>()); // Replace the empty linked list with a real one later on...
            		}
            		// The document is invalid...
            		else {
            			System.out.println("*** ERROR: " + toTrain.getPath().toString() + " is not valid!");
            		}
            		//library.display();
            	}
            }
        }
        catch( ParseException exp ) {
            // Output if something went wrong.
            System.err.println( "Parsing failed. Reason: " + exp.getMessage() );
        }      
    } // End of Main()

	/* ------------------------------------------------------ Methods -------------------------------------------------------- */
	/**
	 * Prints the available ACM classifications to the screen.
	 */
	public static void printOptions() {
		System.out.println("Available Options ->");
		System.out.println("  (A) or (a) --> General Literature");
		System.out.println("  (B) or (b) --> Hardware");
		System.out.println("  (C) or (c) --> Computer Systems Organization");
		System.out.println("  (D) or (d) --> Software");
		System.out.println("  (E) or (e) --> Data");
		System.out.println("  (F) or (f) --> Theory of Computation");
		System.out.println("  (G) or (g) --> Mathematics of Computing");
		System.out.println("  (H) or (h) --> Information Systems");
		System.out.println("  (I) or (i) --> Computing and Methodology");
		System.out.println("  (J) or (j) --> Computing Applications");
		System.out.println("  (K) or (k) --> Computing Milieux\n");
	}
	
	/**
	 * Populates a hash map with the ACM classification options and their accompanying values.
	 * @return map A populated hash map.
	 */
	public static HashMap<String, String> populateMap() {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("a", "General Literature");
		map.put("b", "Hardware");
		map.put("c", "Computer Systems Organization");
		map.put("d", "Software");
		map.put("e", "Data");
		map.put("f", "Theory of Computation");
		map.put("g", "Mathematics of Computing");
		map.put("h", "Information Systems");
		map.put("i", "Computing and Methodology");
		map.put("j", "Computing Applications");
		map.put("k", "Computing Milieux");
		
		return map;
	}
	
	/**
	 * Returns {@link Library} with the information from a new document with a 
	 * predetermined classification. throwOutWords.txt gets updated.
	 * 
	 * @param classifiedDoc A {@link Document} that has a classification other than 
	 * "noClass"
	 * @param  lib A {@link Library} to be updated
	 * @return lib The updated {@link Library}
	 * 
	 * @author awojtowi
	 * @throws IOException 
	 * @see {@link Document}
	 * @see {@link Library}
	 */
    public static Library train(Document classifiedDoc, Library lib) throws IOException {
    	Set<String> docKeys = classifiedDoc.getDocSig().keySet();
    	int isThere = 0;
    	for(String k : docKeys) {
    		if(lib.getThrowOutWords().contains(k)) {
    			isThere++;
    		}
    		else if(classifiedDoc.getDocSig().get(k) < 5) {
    			isThere++;
    		}
    	}
    	if(isThere == docKeys.size()) {
    		System.out.println("WARNING! This file cannot be trained. Not enough significant words!");
    	}
    	
    	else {
    		HashMap<String, HashMap<String, Integer>> theLibrary = lib.getClassMap();
    		HashMap<String, Integer> theClass = theLibrary.get(classifiedDoc.getClassification());
    		HashMap<String, Integer> toAdd = classifiedDoc.getDocSig();
    		classifiedDoc = pruneDocSig(classifiedDoc, lib);
    	
    		String[] classifications = new String[] {"General Literature","Hardware",
    			"Computer Systems Organization", "Software","Data",
    			"Theory of Computation","Mathematics of Computing", 
    			"Information Systems", "Computing and Methodology", 
    			"Computing Applications","Computing Milieux"};
    	
    		// This should update the classification the document belongs to.
    		Set<String> wordsToAdd = toAdd.keySet();
    		for(String i : wordsToAdd) {
    			if(!theClass.containsKey(i)) {
    				theClass.put(i, toAdd.get(i));
    			}
    			else {
    				int currValue = theClass.get(i);
    				theClass.remove(i);
    				theClass.put(i, currValue + toAdd.get(i));
    			}
    		}
    		String keyToPut = classifiedDoc.getClassification();
    		HashMap<String, Integer> mapToPut = theClass;
    	
    		Set<String> otherClasses = new HashSet<String>();
    		String toRemove = classifiedDoc.getClassification();
    		for(int i=0; i<classifications.length; i++) {
    			if(!classifications[i].equals(toRemove)) {
    				otherClasses.add(classifications[i]);
    			}
    		}
    		for(String i : otherClasses) {
    			toAdd = classifiedDoc.getDocSig();
    			wordsToAdd = toAdd.keySet();
    			theClass = theLibrary.get(i);
    			for(String k : wordsToAdd) {
    				if(!theClass.containsKey(k)) {
    					theClass.put(k, 0);
    				}
    			}
    			theLibrary.put(i, theClass);
    		}
    		theLibrary.put(keyToPut, mapToPut);
    		lib.setClassMap(theLibrary);
    		lib.normalize();
    		if(!lib.findPrune(lib.getClassMap()).isEmpty()) {
    			lib.addToThrowOutWordsFile(lib.findPrune(lib.getClassMap()));
    		}
    		lib.prune(lib.getClassMap()); 
    	}
    	return lib;
	}
    
    /**
     * Prunes specific words out of a document signature contained in 
     * Library::throwOutWords when Given a {@link Document} with a 
     * signature.
     * 
     * @param doc A {@link Document}
     * @param lib An instance of the current {@link Library} class
     * 
     * @return doc A {@link Document with pruned document signature
     * @since 1.0
     * @author awojtowi
     */
    public static Document pruneDocSig(Document doc, Library lib) {
		HashMap<String, Integer> currMap = doc.getDocSig();
    	LinkedList<String> toDiscard = lib.getThrowOutWords();
    	for(int i=0; i<toDiscard.size(); i++) {
    		if(currMap.containsKey(toDiscard.get(i))) {
    			currMap.remove(toDiscard.get(i));
    		}
    	}
    	doc.setDocSig(currMap);
    	return doc;
    }
    
    /**
     * Updates the {@link Library} externally. The new state of the library will be 
     * saved by overwriting Library::libraryFile
     * 
     * @param lib A {@link Library} to write
     * @param wordsToThrowOut A {@link Set} of words to write
     * 
     * @since 1.0
     * @author awojtowi
     * @throws IOException 
     */
    public static void finish(Library lib, List<String> wordsToThrowOut) throws IOException {
    	System.out.println("Saving library state in: " + lib.getLibraryFile().toString());
    	lib.addToThrowOutWordsFile(wordsToThrowOut); // This is might become useless...
    	//System.out.println(wordsToThrowOut);
    	if(lib.getClassMap().get("General Literature").isEmpty()) {
    		HashMap<String, Integer> val = new HashMap<String, Integer>();
    		val.put("a", 0);
    		HashMap<String, HashMap<String, Integer>> resetMap = new HashMap<String, HashMap<String, Integer>>();
    		for(int i=0; i<lib.getClassifications().length; i++) {
    			resetMap.put(lib.getClassifications()[i], val);
    		}
    		lib.setClassMap(resetMap);
    	}
    	lib.writeLibrary();
    }
}