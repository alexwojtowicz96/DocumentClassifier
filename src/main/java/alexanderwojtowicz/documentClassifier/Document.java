package alexanderwojtowicz.documentClassifier;

import java.io.File;

import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.io.RandomAccessFile;
//import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.io.RandomAccessRead;
import org.apache.pdfbox.pdfparser.*;
import org.apache.pdfbox.text.PDFTextStripper;;

/**
 * The <code>Document</code> is a container for text that serves as a resource of some sort of 
 * information that can then be categorized based on that information. A <code>Document</code> 
 * falls into one of the following categories for the purposes of this Class:
 * <ul>
 * <li> PDF,
 * <li> ASCII, or
 * <li> other.
 * </ul>
 * A <code>Document</code> holds information on classification and document signature; however, they 
 * are not created upon initialization, rather they are merely contained to later be defined. A 
 * <code>Document</code> must be validated in order to create a document signature, but a 
 * <code>Document</code> may exist that are invalid.
 * 
 * @author Travis Shumaker
 * @since 1.0
 */
public class Document {
//* --------------------------------------------------- Constructors -------------------------------------------------- *//
	/**
	 * Creates a new <code>Document</code> instance with default initialized 
	 * values.
	 * 
	 * @throws IOException If an input or output exception occurred
	 * @author awojtowi
	 * @since 1.0
	 */
	public Document() throws IOException {
		wordCount        = 0;
		docLength        = 0;
		fileType         = "noType";
		classification   = "noClass";
		
		// This is how to find **/Blue1/src/test/data/* correctly every time.
		String classLoc = Document.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		String tests = "../../../../src/test/data/";
		String testDir = (classLoc + tests);
		
		path   = new File(testDir + "nothing.txt"); // Invalid path.
		docSig = new HashMap<String, Integer>();    // Empty docSig.
		System.err.println("WARNING: File is Invalid!");
	}
	
	/**
	 * Creates a new <code>Document</code> instance with the given supplied 
	 * path. If the given filename is empty, then the result is the empty 
	 * abstract pathname.
	 * 
	 * @param p The relative file pathname
	 * 
	 * @throws IOException If an input or output exception occurred
	 * @since 1.0
	 * @author awojtowi
	 */
	public Document(File p) throws IOException {
		System.out.println("Attempting to load file: " + p);
		path = convertToAbsolutePath(p);
		findFileType();
		findDocLength();
		docSig = new HashMap<String, Integer>();
		createHashMap();     
		findWordCount();   
		classification = "noClass";		
		
		if(!isValid()) {
			System.err.println("WARNING: File is Invalid!");
		}
	}
	
	/**
	 * Create a new <code>Document</code> instance with a given file path and known 
	 * classification. This is for if we are trying to train the system with
	 * a known class.
	 * 
	 * @param p The relative file pathname.
	 * @param c The full name Classification of specified <code>Document</code>
	 * 
	 * @throws IOException If an input or output exception occurred
	 * @since 1.0
	 * @author awojtowi
	 */
	public Document(File p, String c) throws IOException {
		System.out.println("Attempting to load file: " + p);
		path = convertToAbsolutePath(p);
		findFileType();
		findDocLength();
		docSig = new HashMap<String, Integer>();
		createHashMap();     
		findWordCount();   
		classification = c;	
		
		if(!isValid()) {
			System.err.println("WARNING: File is Invalid!");
		}
	}
	
	/**
	 *  Create a new <code>Document</code> instance with a all attributes already known.
	 *  <p>
	 *  The <code>filePath</code> is taken and converted into an absolute path 
	 *  and used for the {@link DocumentSignature}. All other values are
	 *  initialized into the <code>Document</code> rather than given base values.
	 *  
	 *  @param p The relative file pathname.
	 *  @param wc Number of words in <code>Document</code>
	 *  @param dl Number of pages in <code>Document</code>
	 *  @param ft The type of file. PDF, ASCII, or other.
	 *  @param c The full name classification of specified <code>Document</code>
	 *  
	 *  @throws IOException If an input or output exception occurred
	 *  @since 1.0
	 *  @author awojtowi
	 */
	public Document(File p, int wc, int dl, String ft, String c) throws IOException {
		path           = convertToAbsolutePath(p);
		wordCount      = wc;
		docLength      = dl;
		fileType       = ft;
		classification = c;
		docSig = new HashMap<String, Integer>(); // Hash map has to be set for this one.
		//createHashMap();  
		
		if(!isValid()) {
			System.err.println("WARNING: File is Invalid!");
		}
	}
	
//* ----------------------------------------------------- Methods ----------------------------------------------------- *//	
	/**
	 *  Converts a relative path to absolute path. This is not altering the path member. To 
	 *  alter the path member pass the path member in instead.
	 *  
	 *  @param thePath The relative path to be converted.
	 *  
	 *  @throws IOException If an input or output exception occurred
	 *  @return thePath The relative path to be converted.
	 *  @since 1.0
	 *  @author awojtowi
	 */
	public File convertToAbsolutePath(File thePath) throws IOException {
		if(!thePath.exists()) {
			printError("convertToAbsolutePath()", "File does not exist");
		}
		else {
			thePath = thePath.getCanonicalFile();
		}
		return thePath;
	}
	
	/**
	 *  Finds the total number of words in the <code>Document</code>.
	 *  <p>
	 *  The <code>Document</code> must be less then or equal to fifty pages in length in 
	 *  order to count the number of words. Stores result in wordCount.
	 *  
	 *  @since 1.0
	 *  @author awojtowi
	 */
	public void findWordCount() {
		if(docSig.equals(new HashMap<String, Integer>())) {
			printError("findWordCount()", "Cannot compute word count for empty hash map");
		}
		else {
			int count = 0;
			Set<String> keys = docSig.keySet();
			for(String i : keys) {
				count = count + docSig.get(i);
			}
			wordCount = count;
		}
	}
	
	/**
	 *  Finds the total number of pages in the <code>Document</code>.
	 *  <p>
	 *  The <code>Document</code> must be between one and fifty pages in length. If it falls
	 *  out of these parameters then an error message alerts the user on the console. Stores
	 *  the result in docLength.
	 *  
	 *  @throws IOException If an input or output exception occurred
	 *  @since 1.0
	 *  @author awojtowi
	 */
	public void findDocLength() throws IOException {
		if(fileType.equals("PDF")) {
			if(!path.exists()) {
				printError("findDocLength()", "PDF file does not exist");
			}
			else {
				PDDocument pdf = PDDocument.load(path);
				if(pdf.getNumberOfPages() > 50) {
					printError("findDocLength()", "PDF file exceeds 50 pages");
				}
				else {
					docLength = pdf.getNumberOfPages();
				}
				pdf.close();
			}
		}
		else if(fileType.equals("ASCII Text")) {
			if(!path.exists()) {
				printError("findDocLength()", "ASCII Text file does not exist");
			}
			else {
				docLength = 1;
			}
		}
		else { 
			if(!path.exists()) {
				printError("findDocLength()", "File does not exist");
			}
			else printError("findDocLength()", "File exists, but is invalid type");
		}
	}
	
	/**
	 *  Find if the file is PDF, ASCII Text, or neither, and stores the result in fileType
	 *  
	 *  @since 1.0
	 *  @author awojtowi
	 */
	public void findFileType() {
		if(path.exists()) {
			if((FilenameUtils.getExtension(path.toString())).equals("txt")) {
				fileType = "ASCII Text";
			}
			else if((FilenameUtils.getExtension(path.toString())).equals("pdf")) {
				fileType = "PDF";
			}
			else { 
				fileType = "noType";
				printError("findFileType", "File is invalid");
			}
		}
		else {
			printError("findFileType()", "File does not exist");
			fileType = "noType";
		}
	}
	
	/**
	 *  Displays all attributes of the <code>Document</code> to the display window.
	 *  
	 *  @since 1.0
	 *  @author awojtowi
	 */
	public void display() {
		if(!isValid()) {
			printError("Document::display()", "Cannot display invalid document");
		}
		else {
			System.out.println();
			System.out.println("DOC PATH     :   " + getPath());
			System.out.println("FILE TYPE    :   " + fileType);
			System.out.println("PAGE COUNT   :   " + docLength + " pages");
			System.out.println("WORD COUNT   :   " + wordCount + " words");
			System.out.println("ACM CLASS    :   " + classification);
			System.out.println("*** DOCUMENT SIGNATURE ***");
			displayDocSig();
			//System.out.println();
		}
	}
	
	/**
	 *  Check the validity of a <code>Document</code>. Checks its length to be within one to fifty 
	 *  pages. Checks file type to be either ASCII text or PDF. Checks if the Document exists.
	 *  
	 *  @return true if this document exists, is between one and fifty pages, and is either 
	 *  an ASCII text or PDF file. false otherwise.
	 *  @since 1.0
	 *  @author awojtowi
	 */
	public boolean isValid() {
		return (checkFileExists() && checkFileSupport() && checkFileLength());
	}
	
	/**
	 *  Indicates whether or not the Document file exists.
	 *  
	 *  @return true if the path exists. false otherwise.
	 *  @since 1.0
	 *  @author awojtowi
	 */
	public boolean checkFileExists() {
		return path.exists();
	}
	
	/**
	 * Indicates if the file is PDF, ASCII Text, or neither
	 * 
	 * @return flag true if file has the extension txt or pdf. false otherwise.
	 * @since 1.0
	 * @author awojtowi
	 */
	public boolean checkFileSupport() {
		boolean flag = true;  // If file is invalid, set to false.
		if(path.exists()) {
			if((FilenameUtils.getExtension(path.toString())).equals("txt")) {
				flag = true;
			}
			else if((FilenameUtils.getExtension(path.toString())).equals("pdf")) {
				flag = true;
			}
			else { // File is invalid
				flag = false;
			}
		}
		else {
			flag = false;
			printError("checkFileSupport()", "File does not exist");
		}
		return flag;
	}
	
	/**
	 * Indicates whether the document is one to fifty pages in length.
	 * 
	 * @return true if the document is one to fifty pages in length.
	 * @since 1.0
	 * @author awojtowi
	 */
	public boolean checkFileLength() {
		if(docLength == 0) {
			return false;
		}
		else if(path.exists()) {
			return ((docLength > 0) && (docLength < 51)); 
		}
		else {
			printError("checkFileLength()", "File does not exist");
			return false;
		}
	}
	
	/**
	 * Prints an error to the debugging output.
	 * 
	 * @param methodName The method with the error
	 * @param explanation explanation of error
	 * @author awojtowi
	 */
	public void printError(String methodName, String explanation) {
		System.err.println("ERROR in method: '" + methodName + "'");
		System.err.println("*** " + explanation + "!");
		//System.out.println();
	}
	
	/**
	 * Create a hashmap where every unique word is the key. It is created based on the content of
	 * the document. It creates the keys based on the words and the value is the frequency in which
	 * it appears in the document.
	 * 
	 * @throws IOException If an input or output exception occurred
	 * @since 1.0
	 * @author awojtowi
	 */
	public void createHashMap() throws IOException {
		if(isValid()) {
			docSig = new HashMap<String, Integer>();
			LinkedList<Character> validChars = new LinkedList<Character>();
			validChars.add('a'); validChars.add('b'); validChars.add('c'); validChars.add('d');
			validChars.add('e'); validChars.add('f'); validChars.add('g'); validChars.add('h');
			validChars.add('i'); validChars.add('j'); validChars.add('k'); validChars.add('l');
			validChars.add('m'); validChars.add('n'); validChars.add('o'); validChars.add('p');
			validChars.add('q'); validChars.add('r'); validChars.add('s'); validChars.add('t');
			validChars.add('u'); validChars.add('v'); validChars.add('w'); validChars.add('x');
			validChars.add('y'); validChars.add('z'); validChars.add('0'); validChars.add('1');
			validChars.add('2'); validChars.add('3'); validChars.add('4'); validChars.add('5');
			validChars.add('6'); validChars.add('7'); validChars.add('8'); validChars.add('9');
			
			if(fileType.equals("ASCII Text")) {
				Scanner reader = new Scanner(new File(path.toString()));
				while(reader.hasNext()) {
					String currWord = reader.next().toLowerCase();
					char[] wordArray = currWord.toCharArray();
					LinkedList<String> splitWords = new LinkedList<String>();
					String validWord = "";
					for(int i=0; i<wordArray.length; i++) {
						if(validChars.contains(wordArray[i])) {
							validWord = validWord + wordArray[i];
						}
						else {
							splitWords.add(validWord);
							validWord = "";
						}
					}
					if(!splitWords.isEmpty()) {
						splitWords.add(validWord);
					}
					// The word was not split...
					if(splitWords.equals(new LinkedList<String>())) {
						if((docSig.isEmpty()) && (!validWord.equals(""))) {
							docSig.put(validWord, 1);
						}
						else if((docSig.containsKey(validWord)) && (!validWord.equals(""))) {
							docSig.put(validWord, (docSig.get(validWord) + 1));
						}
						else {
							if(!validWord.equals("")) {
								docSig.put(validWord, 1);
							}
						}
					}
					// The word was split into sub-words separated by invalid chars.
					else {
						for(int i=0; i<splitWords.size(); i++) {
							if((docSig.isEmpty()) && (!splitWords.get(i).equals(""))) {
								docSig.put(splitWords.get(i), 1);
							}
							else if((docSig.containsKey(splitWords.get(i)) && (!splitWords.get(i).equals("")))) {
								docSig.put(splitWords.get(i), docSig.get(splitWords.get(i)) + 1);
							}
							else {
								if(!splitWords.get(i).equals("")) {
									docSig.put(splitWords.get(i), 1);
								}
							}
						}
					}
				}
				reader.close();
			}
			else if(fileType.equals("PDF")) {
				validChars.add('A'); validChars.add('B'); validChars.add('C'); validChars.add('D');
				validChars.add('E'); validChars.add('F'); validChars.add('G'); validChars.add('H');
				validChars.add('I'); validChars.add('J'); validChars.add('K'); validChars.add('L');
				validChars.add('M'); validChars.add('N'); validChars.add('O'); validChars.add('P');
				validChars.add('Q'); validChars.add('R'); validChars.add('S'); validChars.add('T');
				validChars.add('U'); validChars.add('V'); validChars.add('W'); validChars.add('X');
				validChars.add('Y'); validChars.add('Z');
				/*
				PDDocument document = new PDDocument();
				PDFTextStripper stripper = new PDFTextStripper();
				String documentText = stripper.getText(document.load(path));
				*/
				// FROM: https://www.baeldung.com/pdf-conversions-java
				String documentText = "";
				PDFParser parser = new PDFParser(new RandomAccessFile(path, "r"));
				parser.parse();
				COSDocument cosDoc = parser.getDocument();
				PDFTextStripper stripper = new PDFTextStripper();
				PDDocument document = new PDDocument(cosDoc);
				documentText = stripper.getText(document);
				cosDoc.close();
				document.close();
				
				String currWord = "";
				LinkedList<String> splitWords = new LinkedList<String>();
				char[] entireTextArray = documentText.toCharArray();
				for(int i=0; i<entireTextArray.length; i++) {
					if((entireTextArray[i] == '\t') || (entireTextArray[i] == '\n') || (entireTextArray[i] == ' ')) {
						// The word was not split...
						if(splitWords.equals(new LinkedList<String>())) {
							if((docSig.isEmpty()) && (!currWord.equals(""))) {
								docSig.put(currWord.toLowerCase(), 1);
							}
							else if((docSig.containsKey(currWord.toLowerCase())) && (!currWord.equals(""))) {
								docSig.put(currWord.toLowerCase(), (docSig.get(currWord.toLowerCase())) + 1);
							}
							else {
								if(!currWord.equals("")) {
									docSig.put(currWord.toLowerCase(), 1);
								}
							}
						}
						// The word was split...
						else {
							if(!currWord.equals("")) {
								splitWords.add(currWord);
							}
							for(int k=0; k<splitWords.size(); k++) {
								if((docSig.isEmpty()) && (!splitWords.get(k).equals(""))) {
									docSig.put(splitWords.get(k).toLowerCase(), 1);
								}
								else if((docSig.containsKey(splitWords.get(k).toLowerCase())) && (!splitWords.get(k).equals(""))) {
									docSig.put(splitWords.get(k).toLowerCase(), docSig.get(splitWords.get(k).toLowerCase()) + 1);
								}
								else {
									if(!splitWords.get(k).equals("")) {
										docSig.put(splitWords.get(k).toLowerCase(), 1);
									}
								}
							}
							splitWords = new LinkedList<String>();
						}
						currWord = "";
					}
					else if(!validChars.contains(entireTextArray[i])) {
						splitWords.add(currWord);
						currWord = "";
					}
					else {
						currWord = currWord + entireTextArray[i];
					}
				}
			}
			else {
				printError("createHashMap()", "The document is valid, but fileType was not set to 'ASCII Text' or 'PDF'");
			}
		}
		else {
			printError("createHashMap()", "The document is invalid. No hash map can be created");
		}
	} // End of createHashMap() -------------------------------------------------------------------------------------------
	
	/**
	 * Display the document signature.
	 * @author awojtowi
	 */
	public void displayDocSig() {
		System.out.println("**********************************************");
		// FROM: https://www.youtube.com/watch?v=z5tZ0Zb5rJQ
		Set<String> keys = docSig.keySet();
		for(String i : keys) {
			System.out.println(i + " : " + docSig.get(i));
		}
		System.out.println("**********************************************");
	}
		
//*** ----------------------------------------------------- Getters ----------------------------------------------------- ***//
	public File getPath() {
		return path;
	}
	
	public int getWordCount() {
		return wordCount;
	}
	
	public int getDocLength() {
		return docLength;
	}
	
	public String getFileType() {
		return fileType;
	}
	
	public String getClassification() {
		return classification;
	}
	
	public HashMap<String, Integer> getDocSig() {
		return docSig;
	} 
	
//* ----------------------------------------------------- Setters ----------------------------------------------------- *//
// NOTE: Setters will most likely not be used in any aspect of the final program.
// This is because attributes of a document need to be calculated based on the given file path.
	
	public void setPath(File p) throws IOException {
		if(p.exists()) {
			p = p.getCanonicalFile();
			path = p;
		}
		else { printError("setPath()", "File does not exist"); }
	}
	
	public void setWordCount(int wc) {
		if(wc < 0) {
			printError("setWordCount()", "Word count cannot be < 0");
		}
		else { wordCount = wc; }
	}
	
	public void setDocLength(int dl) {
		if((dl < 1) || (dl > 50)) {
			printError("setDocLength()", "File length is not within range");
		}
		else { docLength = dl; }
	}
	
	public void setFileType(String ft) {
		if((ft.equals("PDF")) || (ft.equals("ASCII Text"))) {
			fileType = ft;
		}
		else { printError("setFileType()", "File type not supported"); }
	}
	
	public void setClassification(String c) {
		if((c.equals("General Literature")) || (c.equals("Hardware"))
				|| (c.equals("Computer Systems Organization")) || (c.equals("Software"))
				|| (c.equals("Data")) || (c.equals("Theory of Computation"))
				|| (c.equals("Mathematics of Computing")) || (c.equals("Information Systems"))
				|| (c.equals("Computing and Methodology")) || (c.equals("Computing Applications"))
				|| (c.equals("Computing Milieux"))) {
			classification = c;
		}
		else { printError("setClassification()", "Classification name does not exist"); }
	}
	
	public void setDocSig(HashMap<String, Integer> sig) {
		docSig = sig;
	}
	
//*** ---------------------------------------------- Private Member Data ------------------------------------------------ ***//
	private File path;                          // Canonical path of the document.
	private int wordCount;                      // Number of words in the document.
	private int docLength;                      // Number of pages in the document.
	private String fileType;                    // PDF or ASCII Text
	private String classification;              // The classification of a document
	private HashMap<String, Integer> docSig;    // A collection of words : freq in a document.
	
} // End of Document.java