package alexanderwojtowicz.documentClassifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;
import java.util.LinkedList;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * The <code>Library</code> contains the classified files and the words to 
 * disregard when classifying a {@link Document}
 * 
 * @since 1.0
 * @author tshumake
 * @see Document
 */
public class Library {
	
/* ***************************************************************************************************************** */
	/**
	 * Create a <code>Library</code> based on the libraryFile. It also creates 
	 * a list of words that are discarded during the classify process based on 
	 * lack of ability to use that word to classify it into a classification, 
	 * and stores that list in throwOutWords.
	 * 
	 * @throws IOException If an input or output exception occurred.
	 * @since 1.0
	 * @author awojtowi
	 */
	public Library() throws IOException {		
		// This classLoc is the location of Classify-0.1.jar.
		String classLoc   = Library.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		String fileLoc    = classLoc + "../../../../src/main/resources/";		

		// Location of throwOutWords.txt
		throwOutWordsFile = new File(fileLoc + "throwOutWords.txt").getCanonicalFile();  

		// Location of library.txt
		libraryFile = new File(fileLoc + "/library.txt").getCanonicalFile();        
		
		// Initialize throwOutWords to contents of **/throwOutWords.txt
		throwOutWords = new LinkedList<String>();
		Scanner reader = new Scanner(throwOutWordsFile);
		while(reader.hasNext()) {
			String currWord = reader.next().toLowerCase();
			throwOutWords.add(currWord);
		}
		reader.close();
		if(throwOutWords.equals(new LinkedList<String>())) {
			System.err.println("WARNING: throwOutWords.txt is empty!");
		}
		
		// This will update the library every time we boot up the program.
		classMap = new HashMap<String, HashMap<String, Integer>>();
		readLibrary();  
	}
	
/* ***************************************************************************************************************** */
	/**
	 * Updates classMap based on the libraryFile.
	 * 
	 * @since 1.0
	 * @author dholter
	 * @throws IOException 
	 */
	public void readLibrary() throws IOException {
		if((libraryFile.exists()) && (libraryFile.canRead())) {
			BufferedReader reader = new BufferedReader(new FileReader(libraryFile));
			if(reader.readLine() == null) {
			    System.err.println("WARNING: The library is empty!");
			}
			else { 
				classMap = new HashMap<String, HashMap<String,Integer>>();
				Scanner s = new Scanner(libraryFile);
				int count = 0;
				String[] words = s.nextLine().trim().split("\\s+");
				while(s.hasNextLine()) {
					String[] currentLine = s.nextLine().trim().split("\\s+");
					classMap.put(classifications[count], new HashMap<String, Integer>());
					for(int i = 0; i < words.length; i++) {
						classMap.get(classifications[count]).put(words[i], Integer.parseInt(currentLine[i]));
					}
					count++;
				}			
				s.close();
			}
			reader.close();
		}
		else {
			if(!libraryFile.exists()) {
				System.out.println("*** ERROR: Library not found!");
			}
			if(!libraryFile.canRead()) {
				System.out.println("*** ERROR: Library cannot be read!");
			}
		}
	}
	
	/**
	 * Reads the current file in throwOutWordsFile into throwOutWords linked list.
	 * 
	 * @since 1.0
	 * @author awojtowi
	 * @throws FileNotFoundException 
	 */
	public void readThrowOutWords() throws FileNotFoundException {
		throwOutWords = new LinkedList<String>();
		Scanner reader = new Scanner(throwOutWordsFile);
		while(reader.hasNext()) {
			throwOutWords.add(reader.next());
		}
		reader.close();
	}
	
	/**
	 * Writes classMap to libraryFile.
	 * 
	 * @since 1.0
	 * @author dholter
	 * @throws FileNotFoundException  
	 */
	public void writeLibrary() throws FileNotFoundException {
		Set<String> words = classMap.get("General Literature").keySet();
		PrintWriter writer = new PrintWriter(libraryFile);
		for(String i: words) {
			writer.print(i + " ");
		}
		writer.println();
		for(int i=0; i<classifications.length; i++) {
			for(String k : words) {
				writer.print(classMap.get(classifications[i]).get(k) + " ");
			}
			writer.println();
		}
		writer.close();
	}
	
	/**
	 * Finds words that are to be disregarded in the classify process.
	 * 
	 * @param classMap The library
	 * 
	 * @return  List<String> words to be pruned from the library.
	 * @since 1.0
	 * @author dholter
	 * @see Classify
	 */
	/*
	public List<String> findPrune(HashMap<String, HashMap<String, Integer>> classMap) {
		List<String> toPrune = new Vector<String>();
		Entry<String, HashMap<String, Integer>> classIt = classMap.entrySet().iterator().next();
		Iterator<Entry<String, Integer>> wordIt = (classIt.getValue()).entrySet().iterator();
		
		while(wordIt.hasNext()) {
			Entry<String, Integer> wordtest = wordIt.next();
			boolean breakLoop = false;
			Set <String> keys = classMap.keySet();
			for(String i:keys) {
				if(classMap.get(i).get(wordtest.getKey())==0)
					breakLoop = true;
					break;
			}
			if(breakLoop) {
				break;
			}
			toPrune.add(wordtest.getKey());
		}
		return toPrune;
	}*/
	public List<String> findPrune(HashMap<String, HashMap<String, Integer>> classMap) {
		List<String> toPrune = new Vector<String>();
		Set<String> allWords = classMap.get("General Literature").keySet();
		Set<String> classes = classMap.keySet();
		
		for(String w : allWords) {
			int exists = 0;
			for(String c : classes) {
				if(classMap.get(c).get(w) != 0) {
					exists++;
				}
			}
			if(exists == classMap.size()) {
				toPrune.add(w);
			}
		}
		
		return toPrune;
	}
	
	/**
	 * Return an updated library that has been pruned.
	 * 
	 * @param classMap The library before pruning
	 * 
	 * @return classMap The library after pruning
	 * @since 1.0
	 * @author dholter
	 */
	/*
	public HashMap<String, HashMap<String, Integer>> prune(HashMap<String, HashMap<String, Integer>> classMap) {
		List<String> toPrune = new Vector<String>();
		toPrune = findPrune(classMap);
		Iterator<String> toPruneIt = toPrune.iterator();
		
		while(toPruneIt.hasNext()) {
			String removeWord = toPruneIt.next();
			Iterator<Entry<String, HashMap<String, Integer>>> classIt = classMap.entrySet().iterator();
			while(classIt.hasNext()) {
				Entry<String, HashMap<String, Integer>> removeFrom = classIt.next();
				removeFrom.getValue().remove(removeWord);
			}
		}
		return classMap;
	}*/
	public HashMap<String, HashMap<String, Integer>> prune(HashMap<String, HashMap<String, Integer>> classMap) {
		List<String> toPrune = new Vector<String>();
		toPrune = findPrune(classMap);
		if(!toPrune.isEmpty()) {
			Set<String> classes = classMap.keySet();
			for(int i=0; i<toPrune.size(); i++) {
				for(String c : classes) {
					classMap.get(c).remove(toPrune.get(i));
				}
			}
		}
		
		return classMap;
	}
	
	/**
	 * Adds new word(s) to throwOutWords.txt. This file is full of words deemed 
	 * not useful for finding the classification of a {@link Document}.
	 * 
	 * @param newWords word to be added to throwOutWords.txt 
	 * 
	 * @throws IOException If an input or output exception occurred.
	 * @since 1.0
	 * @author awojtowi
	 * @see Document
	 */
	public void addToThrowOutWordsFile(List<String> newWords) throws IOException {
		LinkedList<String> list = new LinkedList<String>();
		LinkedList<String> toAdd = new LinkedList<String>();
		// In case there are duplicates...
		for(int i=0; i<newWords.size(); i++) {
			if(!(toAdd.contains(newWords.get(i)))) {
				toAdd.add(newWords.get(i));
			}
		}
		Scanner reader = new Scanner(throwOutWordsFile);
		while(reader.hasNext()) {
			String currWord = reader.next().toLowerCase();
			if(!(list.contains(currWord))) {
				list.add(currWord);
			}
		}
		reader.close();
		
		if(list.equals(new LinkedList<String>())) {
			// throwOutWords.txt is empty...
			System.err.println("WARNING: '" + throwOutWords.toString() + "' is empty!");
		}
		
		for(int i=0; i<toAdd.size(); i++) {
			if(!(list.contains(toAdd.get(i)))) {
				list.add(toAdd.get(i));
			}
		}
		PrintWriter writer = new PrintWriter(throwOutWordsFile);
		writer.flush();
		for(int i=0; i<list.size(); i++) {
			writer.println(list.get(i));
		}
		writer.close();
		setThrowOutWords(list);
	}
	
	/**
	 * Display the library contents and the throwOutWords list.
	 * Also display the paths of the library and throwOutWords files.
	 * 
	 * @since 1.0
	 * @author awojtowi
	 * @throws IOException 
	 */
	public void display() throws IOException {
		System.out.println("\n-------------------- LIBRARY --------------------");
		Set<String> wordKeys;
		for(int i=0; i<classifications.length; i++) {
			wordKeys = classMap.get(classifications[i]).keySet();
			System.out.println("===== " + classifications[i] + " =====");
			for(String k : wordKeys) {
				System.out.print(k + " : " + classMap.get(classifications[i]).get(k) + "\n");
			}
			if(!(i == (classifications.length - 1))) {
				System.out.println();
			}
		}
		System.out.println("-------------------------------------------------\n");
		System.out.println("----------------- IGNORED WORDS -----------------");
		for(int i=0; i<throwOutWords.size(); i++) {
			System.out.println(throwOutWords.get(i));
		}
		System.out.println("-------------------------------------------------\n");
		System.out.println("------------------- FILE PATHS ------------------");
		System.out.println("Library.txt --------> " + libraryFile.getCanonicalFile().toString());
		System.out.println("throwOutWords.txt --> " + throwOutWordsFile.getCanonicalFile().toString());
		System.out.println("-------------------------------------------------\n");
	}
	
	/**
	 * Prune the library of any words that do not appear at least 5 times in any classification.
	 * 
	 * @since 1.0
	 * @author awojtowi
	 */
	public void normalize() {
		if(classMap.isEmpty()) {
			System.err.println("*** ERROR: The classMap is empty!");
		}
		else {
			LinkedList<String> toDiscard = new LinkedList<String>();
			for(int i=0; i<classifications.length; i++) {
				String currClass = classifications[i];
				Set<String> words = classMap.get(currClass).keySet();
				for(String k : words) {
					if(classMap.get(currClass).get(k) < 5) {
						classMap.get(currClass).put(k, 0);
						//System.out.println(classMap.get(currClass).get(k));
					}
					//System.out.println(classMap.get(currClass).get(k));
				}
			}
			Set<String> allWords = classMap.get("General Literature").keySet();
			for(String i : allWords) {
				int isZero = 0;
				for(int k=0; k<classifications.length; k++) {
					if(classMap.get(classifications[k]).get(i) == 0) {
						isZero++;
					}
				}
				// Prune out the word with 0 entries in every classification...
				if(isZero == classifications.length) {
					toDiscard.add(i);
				}
			}
			for(int i=0; i<toDiscard.size(); i++) {
				allWords.remove(toDiscard.get(i));
			}
			if(allWords.size() == 0) {
				System.err.println("WARNING! The classMap is now empty!");
				classMap = new HashMap<String, HashMap<String, Integer>>();
			}
			else {
				HashMap<String, HashMap<String, Integer>> newMap = new HashMap<String, HashMap<String, Integer>>();
				for(int k=0; k<classifications.length; k++) {
					HashMap<String, Integer> temp = new HashMap<String, Integer>();
					for(String j : allWords) {
						temp.put(j, classMap.get(classifications[k]).get(j));
					}
					newMap.put(classifications[k], temp);
				}
				classMap = newMap;
			}
		}
	}
	
	/**
	 * Generate a .csv file representation of the library
	 * 
	 * @since 1.0
	 * @author awojtowi
	 * @throws IOException 
	 */
	public void generateCsv() throws IOException {
    	final String classLoc = Library.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
    	final String dir = "../../../../src/main/resources/";
    	final String fileDir = (new File(classLoc + dir).getCanonicalFile()).toString();
    	final String ext = ".csv";
    	PrintWriter writer = new PrintWriter(new File(fileDir + "/library" + ext));
    	
    	writer.print("words,");
    	for(int i=0; i<classifications.length; i++) {
    		writer.print(classifications[i]);
    		if(i != (classifications.length - 1)) {
    			writer.print(",");
    		}
    	}
    	writer.println();
    	Set<String> keys = classMap.get("Data").keySet();
    	LinkedList<String> words = new LinkedList<String>();
    	for(String k : keys) {
    		words.add(k);
    	}
    	for(int w=0; w<words.size(); w++) {
    		writer.print(words.get(w) + ",");
    		for(int c=0; c<classifications.length; c++) {
    			if(classMap.get(classifications[c]).get(words.get(w)) != 0) {
    				writer.print(classMap.get(classifications[c]).get(words.get(w)));
    			}
    			if(c != (classifications.length - 1)) {
    				writer.print(",");
    			}
    		}
    		writer.println();
    	}
    	writer.close();
	}
	
	/**
	 * Delete library.csv
	 * 
	 * @since 1.0
	 * @author awojtowi
	 * @throws IOException 
	 */
	public void deleteGeneratedCsv() throws IOException {
    	final String classLoc = Library.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
    	final String dir = "../../../../src/main/resources/";
    	final String fileDir = (new File(classLoc + dir).getCanonicalFile()).toString();
    	final String ext = ".csv";
    	File theFile = new File(fileDir + "/library" + ext);
    	
    	if(!theFile.exists()) {
    		System.err.println("*** ERROR: You tried to delete library.csv but it does not exist!");
    	}
    	else {
    		theFile.delete();
    	}
	}
	
	public void generateArff() throws IOException {
		final String classLoc = Library.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
    	final String dir = "../../../../src/main/resources/";
    	final String fileDir = (new File(classLoc + dir).getCanonicalFile()).toString();
    	final String ext = ".arff";
    	File theFile = new File(fileDir+"/library"+ext);
    	
    	Set<String> words = classMap.keySet();
    	
    	PrintWriter writer = new PrintWriter(theFile);
    	writer.println("@RELATION library\n");
    	writer.println("@ATTRIBUTE document string");
    	writer.println("@ATTRIBUTE class {General Literature, Hardware, " + 
    			"Computer Systems Organization, Software,Data, " + 
    			"Theory of Computation, Mathematics of Computing, " + 
    			"Information Systems, Computing and Methodology, " + 
    			"Computing Applications, Computing Milieux}\n");
    	writer.println("@DATA");
    	for(int i = 0; i < classifications.length; i++)
    	{
    		String line = "";
    		for(String j: words)
    		{
    			if(classMap.get(classifications[i]).get(j) == null) {
    				continue;
    			}
    			if(classMap.get(classifications[i]).get(j) != 0)
    			{
    				line = line + j + " ";
    			}
    		}
    		if(!line.equals("")) {
    			String newLine = "";
    			char[] arr = line.toCharArray();
    			for(int k=0; k<arr.length-1; k++) {
    				newLine = newLine + arr[k];
    			}
    			line = newLine;
    		}
    		writer.println("\"" + line + "\"," + i);
    	}
    	writer.close();
	}
	
/* ***************************************************************************************************************** */
	public HashMap<String, HashMap<String, Integer>> getClassMap() {
		return classMap;
	}
	
	public String[] getClassifications() {
		return classifications;
	}
	
	public File getThrowOutWordsFile() throws IOException {
		return throwOutWordsFile.getCanonicalFile();
	}
	
	public File getLibraryFile() {
		return libraryFile;
	}
	
	public LinkedList<String> getThrowOutWords() {
		return throwOutWords;
	}
	
	public void setClassMap(HashMap<String, HashMap<String, Integer>> lib) {
		classMap = lib;
	}
	
	public void setThrowOutWords(LinkedList<String> list) {
		throwOutWords = list;
	}
	
	public void setThrowOutWordsFile(File file) {
		throwOutWordsFile = file;
	}
	
	public void setLibraryFile(File file) {
		libraryFile = file;
	}
	
/* ***************************************************************************************************************** */
	private HashMap<String, HashMap<String, Integer>> classMap;   // Holds the union of all document hash maps for every classification.
	private LinkedList<String> throwOutWords;                     // A list of words found to be undesirable for classification.
	private File throwOutWordsFile;                               // Holds information about throwOutWords in memory.
	private File libraryFile;                                     // Holds information about the library (classMap) in memory.
	private String[] classifications = new String[] {"General Literature","Hardware",
			"Computer Systems Organization", "Software","Data",
			"Theory of Computation","Mathematics of Computing", 
			"Information Systems", "Computing and Methodology", 
			"Computing Applications","Computing Milieux"}; // A-K
	
/* ***************************************************************************************************************** */
} // End of Library.java