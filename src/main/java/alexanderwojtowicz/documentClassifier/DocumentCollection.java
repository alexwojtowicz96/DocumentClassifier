package alexanderwojtowicz.documentClassifier;

import java.util.*;
//import java.io.File;
//import java.io.IOException;

/**
 * The <code>Document Collection</code> is a container of {@link Document}
 * <code>s</code>. It is mainly intended to group separate documents into a 
 * single container such that they may all be accessed from a single call. It 
 * is up to the programmer to fill the document collection with valid
 * documents, as a <code>Document Collection</code> will not check it's
 * own validity.
 * 
 * @since 1.0
 * @author tshumake
 */
public class DocumentCollection {
/* ------------------------------------------------- Constructors ------------------------------------------------- */	
	/**
	 * Creates a new <code>DocumentCollection</code> instance without any 
	 * contents.
	 * 
	 * @since 1.0
	 * @author awojtowi
	 */
	public DocumentCollection() {
		documentList = new LinkedList<Document>();
	}
	
	/**
	 * Creates a new <code>DocumentCollection</code> instance with at least 
	 * one document within. If given a 'null' document it will act as 
	 * {@link DocumentCollection()} does.
	 * 
	 * @param docs  Linked list of {@link Document}s objects
	 * 
	 * @since 1.0
	 * @author awojtowi
	 * @see Document
	 */
	public DocumentCollection(LinkedList<Document> docs) {
		documentList = docs;
	}
	
/* ---------------------------------------------------- Methods ---------------------------------------------------- */
	/**
	 * Add a new {@link Document} to the end of documentList.
	 * 
	 * @param doc  The {@link Document} to be added to the end of the 
	 * <code>DocumentCollection</code>
	 * 
	 * @since 1.0
	 * @author awojtowi
	 * @see Document
	 */
	public void addToCollection(Document doc) {
		documentList.add(doc);
	}
	
	/**
	 * Displays each of the {@link Document}(s) in the <code>DocumentList</code>
	 * to the console screen. 
	 * 
	 * @since 1.0
	 * @author awojtowi
	 */
	public void display() {
		for(int i=0; i<documentList.size(); i++) {
			documentList.get(i).display();
			System.out.println();
		}
	}
	
	/**
	 * Displays the classification of every {@link Document} in the 
	 * <code>DocumentCollection</code> to the console screen. 
	 * 
	 * @since 1.0
	 * @author awojtowi
	 */
	public void printClassifications() {
		System.out.println();
		System.out.println("================================= Results =================================");
		for(int i=0; i<documentList.size(); i++) {
			System.out.println("FILE: " + documentList.get(i).getPath().toString());
			System.out.println("  --> " + documentList.get(i).getClassification());
		}
		System.out.println("===========================================================================");
		System.out.println();
	}
	
/* ---------------------------------------------------- Getters ---------------------------------------------------- */
	/**
	 * Returns the collection's list of document's.
	 * 
	 * @since 1.0
	 * @author awojtowi
	 */
	public LinkedList<Document> getDocumentList() {
		return documentList;
	}
	
	/**
	 * Returns the number of entries in documentList.
	 *  
	 * @since 1.0
	 * @author awojtowi
	 */
	public int getSize() {
		return documentList.size();
	}
	
/* ---------------------------------------------------- Setters ---------------------------------------------------- */
	/**
	 * Sets the collection's document list to an existing linked list.
	 * 
	 * @since 1.0
	 * @param docs
	 * @author awojtowi
	 */
	public void setDocumentList(LinkedList<Document> docs) {
		documentList = docs;
	}
	
/* ---------------------------------------------- Private Member Data ---------------------------------------------- */
	private LinkedList<Document> documentList;  // A list of supplied documents (not necessarily valid)
	
} // End of DocumentCollection.java