package alexanderwojtowicz.documentClassifier;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.BasicParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/*import weka.classifiers.Classifier;
import weka.classifiers.trees.J48;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.core.Utils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;
import weka.core.converters.ConverterUtils.DataSource;*/

public class Classifier {
//*** ------------------------------------------------------ MAIN ------------------------------------------------------- ***//	
    @SuppressWarnings("static-access")
	public static void main(String[] args) throws IOException {
    	
    	// Arguments should be done as the first thing.
    	Options options = new Options();
    	Option help = new Option("h", "help", false, "Print the help information."); 
        options.addOption(help);
    	options.addOption(OptionBuilder.withArgName("file").withDescription("File you want to classify.").hasArg().create("file"));
        CommandLineParser parser = new BasicParser();
        
        try {
            // Parse the command line arguments
            CommandLine cmd = parser.parse(options, args);
            String file_value = cmd.getOptionValue("file");
            
            if(cmd.hasOption("help") || args.length == 0 || file_value == null) {
            	HelpFormatter formatter = new HelpFormatter();
            	formatter.printHelp("Classify", options, true);
            }
            else {
            	String[] paths = file_value.split(",");
            	DocumentCollection docCollection = new DocumentCollection(); 
            	LinkedList<File> pathsList = new LinkedList<File>();
            	Library library = new Library();
            	
            	// Add all paths to a temporary list.
            	for(int i =0; i<paths.length; i++) {
            		pathsList.add(new File(paths[i]));
            	}
            	
            	// If it is a valid document, classify and add it to the document collection.
            	for(int i=0; i<pathsList.size(); i++) {
            		Document currDoc = new Document(pathsList.get(i));
            		if(currDoc.isValid()) {
            			// Comment the following line out if you want to see the raw signatures...
            			currDoc = classifyDocument(currDoc, library);
            			docCollection.addToCollection(currDoc);
            			System.out.println("Sucessfully Loaded: " + currDoc.getPath().toString());
            		}
            		else {
            			System.out.println("*** ERROR: Document at '" + currDoc.getPath().toString() + "' is INVALID:");
                		System.out.println("    Document was not added to collection!");
            		}
            	}  // We now have a collection of valid classified documents stored in docCollection.
            	
            	//docCollection.display();  // Print all supplied VALID documents to the screen.
            	docCollection.printClassifications();  // Print the valid documents' paths and classifications.
            }
        }
        catch(ParseException exp) {
            // Output if something went wrong with parsing.
            System.err.println("Parsing failed. Reason: " + exp.getMessage());
        }
    } // End of Main
//*** ------------------------------------------------------------------------------------------------------------------- ***//
//*** ----------------------------------------------------- Methods ----------------------------------------------------- ***//
    /**
     * Classify a single document based on it's document signature.
     * @param A {@link Document} to be classified
     * @param A {@link Library} to reference
     * @return The now-classified {@link Document}
     * @throws IOException 
     */
    public static Document classifyDocument(Document currDoc, Library lib) throws IOException {
    	if(!currDoc.isValid()) {
    		System.out.println("*** ERROR: Document at '" + currDoc.getPath().toString() + "' is INVALID:");
    		System.out.println("    Document could not be classified!");
    	}
    	if(lib.getClassMap().get("General Literature").isEmpty()) {
    		System.out.println("*** ERROR: Library at: " + lib.getLibraryFile().toString());
    		System.out.println("Is your library file initialized?");
    	}
    	else {
    		currDoc = prepareDocument(currDoc, lib);
    		lib = prepareLibrary(currDoc, lib);
    		generate(lib, currDoc);
    		lib.generateCsv();
    		
    		// Classify (This does not use the SVM...)
    		Set<String> keys = lib.getClassMap().get("Data").keySet();
    		LinkedList<String> words = new LinkedList<String>();
    		for(String k : keys) {
    			words.add(k);
    		}
        	double docAvg = 0.00;
        	double[] libAvgs = new double[lib.getClassifications().length];
        	double counter = 0;
        	for(int i=0; i<words.size(); i++) {
        		if(currDoc.getDocSig().get(words.get(i)) != 0) {
        			docAvg = docAvg + currDoc.getDocSig().get(words.get(i));
        			counter++;
        		}
        	}
        	docAvg = (docAvg / counter);
        	for(int i=0; i<lib.getClassifications().length; i++) {
        		counter = 0;
        		for(int w=0; w<words.size(); w++) {
        			int currWordVal = lib.getClassMap().get(lib.getClassifications()[i]).get(words.get(w));
        			if(currWordVal != 0) {
        				libAvgs[i] = libAvgs[i] + currWordVal;
        				counter++;
        			}
        		}
        		libAvgs[i] = (libAvgs[i] / counter);
        	}
    		
        	double[] countArr = new double[lib.getClassifications().length];
        	double[] classifyCoeff = new double[lib.getClassifications().length];
        	double[] rawCoeff = new double[classifyCoeff.length];
        	for(int i=0; i<lib.getClassifications().length; i++) {
        		classifyCoeff[i] = 0;
        		rawCoeff[i] = 0;
        	}
        	for(int c=0; c<lib.getClassifications().length; c++) {
        		double normalFactor = (libAvgs[c] / docAvg);
        		counter = 0;
        		for(int w=0; w<words.size(); w++) {
        			if(currDoc.getDocSig().get(words.get(w)) != 0) {
        				double docWordVal = (currDoc.getDocSig().get(words.get(w)) * normalFactor);
        				double libWordVal = lib.getClassMap().get(lib.getClassifications()[c]).get(words.get(w));
        				double toAdd = (libWordVal - docWordVal);
        				if(toAdd < 0.00) {
        					toAdd = (0.00 - toAdd);
        				}
        				classifyCoeff[c] = classifyCoeff[c] + toAdd;
        				rawCoeff[c] = rawCoeff[c] + toAdd;
        				if(libWordVal < 4) {
        					counter++;
        				}
        			}
        		}
        		countArr[c] = counter;
        		classifyCoeff[c] = (classifyCoeff[c] * counter);
        	}

        	/*
        	System.out.println("\nThe lowest number is the predicted classification.");
        	System.out.println("Counter is the number of words not in that section of the library.");
        	for(int i=0; i<classifyCoeff.length; i++) {
        		String buffer = "...";
        		for(int q=lib.getClassifications()[i].length(); q<29; q++) {
        			buffer = buffer + ".";
        		}
        		System.out.println(lib.getClassifications()[i] + " " + buffer + " classifyCoeff -> " + 
        					classifyCoeff[i] + " | rawCoeff -> " + rawCoeff[i] + " | Counter -> " + countArr[i]);
        	}
        	*/
        	
        	boolean cantClassify = false;
        	int zeros = 0;
        	List<Integer> haveZero = new LinkedList<Integer>();
        	for(int i=0; i<classifyCoeff.length; i++) {
        		if(classifyCoeff[i] == 0.00) {
        			zeros++;
        			haveZero.add(i);
        		}
        	}
        	if(zeros == classifyCoeff.length) {
        		cantClassify = true;
        	}
        	if(!cantClassify) {
        		double min = Double.MAX_VALUE;
        		int index = -1;
        		if(zeros < 2) {
        			for(int i=0; i<classifyCoeff.length; i++) {
        				if(classifyCoeff[i] < min) {
        					min = classifyCoeff[i];
        				}
        			}
        			for(int i=0; i<classifyCoeff.length; i++) {
        				index++;
        				if(classifyCoeff[i] == min) {
        					break;
        				}
        			}
        		}
        		else {
        			for(int i=0; i<haveZero.size(); i++) {
        				if(rawCoeff[haveZero.get(i)] < min) {
        					min = rawCoeff[haveZero.get(i)];
        				}
        			}
        			for(int i=0; i<rawCoeff.length; i++) {
        				index++;
        				if(rawCoeff[i] == min) {
        					break;
        				}
        			}
        		}
    			//System.out.println("MIN_VALUE -> " + min);
    			//System.out.println("INDEX -----> " + index);
    			currDoc.setClassification(lib.getClassifications()[index]);
        	}
        	else {
        		System.out.println("*** This document cannot be classified. The document does not contain enough significant words!");
        	}
    		// Comment this out to view the files. 
    		// Note: They are only valid for the last document in the Document Collection.
    		deleteGeneratedFiles();
    		lib.deleteGeneratedCsv();
    	}
    	return currDoc; 
    }
    
    /**
     * Prune out ignored words and words with a frequency <5 from a {@link Document}'s document signature.
     * 
     * @since 1.0
     * @author awojtowi
     * 
     * @param A {@link Document} to be pruned
     * @param A {@link Library} to reference
     * @return A {@link Document} with a pruned document signature which is now prepared to be classified.
     */
    public static Document prepareDocument(Document currDoc, Library lib) {
    	HashMap<String, Integer> docSig = currDoc.getDocSig();
    	Set<String> docWords = docSig.keySet();
    	Set<String> libWords = lib.getClassMap().get("General Literature").keySet();
    	for(String l : libWords) {
    		if(!docSig.containsKey(l)) {
    			docSig.put(l, 0);
    		}
    	}
    	List<String> toRemove = new LinkedList<String>();
    	for(String d : docWords) {
    		if(docSig.get(d) < 5) {
    			if(!libWords.contains(d)) {
    				toRemove.add(d);
    			}
    			else {
    				docSig.put(d, 0);
    			}
    		}
    	}
    	for(int i=0; i<lib.getThrowOutWords().size(); i++) {
    		if(docSig.containsKey(lib.getThrowOutWords().get(i))) {
    			if(!toRemove.contains(lib.getThrowOutWords().get(i))) {
    				toRemove.add(lib.getThrowOutWords().get(i));
    			}
    		}
    	}
    	if(!toRemove.isEmpty()) {
    		for(int i=0; i < toRemove.size(); i++) {
    			docSig.remove(toRemove.get(i));
    		}
    	}
    	currDoc.setDocSig(docSig);
    	
    	return currDoc;
    }
    
    /**
     * Union a {@link Library} classMap with new words found in currDoc.
     * New words have a frequency of 0 in the returned library.
     * 
     * @since 1.0
     * @author awojtowi
     * 
     * @param A prepared {@link Document}
     * @param A {@link Library} to be prepared
     * @return Prepared {@link Library} with updated classMap
     */
    // For this to work, currDoc needs to be prepared!
    public static Library prepareLibrary(Document currDoc, Library lib) {
    	Set<String> classes = lib.getClassMap().keySet();
    	Set<String> libWords = lib.getClassMap().get("General Literature").keySet();
    	Set<String> docWords = currDoc.getDocSig().keySet();

    	for(String i : docWords) {
    		if(!libWords.contains(i)) {
    			for(String c : classes) {
    				lib.getClassMap().get(c).put(i, 0);
    			}
    		}
    	}
    	if(lib.getClassMap().get("General Literature").containsKey("a")) {
    		int count = 0;
    		for(String c : classes) {
    			if(lib.getClassMap().get(c).get("a") == 0) {
    				count++;
    			}
    		}
    		if(count == lib.getClassMap().size()) {
    			for(String c : classes) {
    				lib.getClassMap().get(c).remove("a");
    			}
    		}
    	}
    	
    	return lib;
    }
    
    /**
     * Generate .csv files for later use with the SVM.
     * 
     * @since 1.0
     * @author awojtowi
     * 
     * @param A prepared {@link Library} to be referenced
     * @param A prepared {@link Document} to be classified
     * @throws IOException 
     */
    public static void generate(Library prepLib, Document prepDoc) throws IOException {
    	// Files are generated with each row as: word,#in_prepDoc,#in_prepLib
    	
    	final String classLoc = Classifier.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
    	final String dir = "../../../../src/main/resources/";
    	final String fileDir = (new File(classLoc + dir).getCanonicalFile()).toString();
    	final String ext = ".csv";
    	
    	Set<String> words = prepDoc.getDocSig().keySet();
    	LinkedList<String> allWords = new LinkedList<String>();
    	for(String w : words) {
    		allWords.add(w);
    	}
    	
    	// In order to get accurate results, we need to normalize the values in the document.
    	double docAvg = 0.00;
    	double[] libAvgs = new double[prepLib.getClassifications().length];
    	double counter = 0;
    	for(int i=0; i<allWords.size(); i++) {
    		if(prepDoc.getDocSig().get(allWords.get(i)) != 0) {
    			docAvg = docAvg + prepDoc.getDocSig().get(allWords.get(i));
    			counter++;
    		}
    	}
    	docAvg = (docAvg / counter);
    	for(int i=0; i<prepLib.getClassifications().length; i++) {
    		counter = 0;
    		for(int w=0; w<allWords.size(); w++) {
    			int currWordVal = prepLib.getClassMap().get(prepLib.getClassifications()[i]).get(allWords.get(w));
    			if(currWordVal != 0) {
    				libAvgs[i] = libAvgs[i] + currWordVal;
    				counter++;
    			}
    		}
    		libAvgs[i] = (libAvgs[i] / counter);
    	}
    	/*
    	System.out.println("........ docAvg -----> " + docAvg);
    	for(int i=0; i<11; i++) {
    		System.out.println("........ libAvgs[" + i + "] -> " + libAvgs[i]);
    	}*/
    	
    	PrintWriter writer;
    	for(int i=0; i<prepLib.getClassifications().length; i++) {
    		double normalFactor = (libAvgs[i] / docAvg);
    		String fName = (fileDir + "/" + prepLib.getClassifications()[i].toLowerCase().replace(' ', '_') + ext);
    		writer = new PrintWriter(new File(fName));
    		for(int w=0; w<allWords.size(); w++) {
    			// Only write tuples for useful words contained in the prepared document...
    			if(prepDoc.getDocSig().get(allWords.get(w)) != 0) {
        			writer.print(allWords.get(w) + "," + (prepDoc.getDocSig().get(allWords.get(w)) * normalFactor) + ",");
        			if(!(prepLib.getClassMap().get(prepLib.getClassifications()[i]).get(allWords.get(w)) == 0)) {
   		                 writer.print(prepLib.getClassMap().get(prepLib.getClassifications()[i]).get(allWords.get(w)));
        			}
        			writer.println();
    			}
    		}
    		writer.close();
    	}	
    }
    
    /**
     * Deletes the files generated in {@link Classify}::generate().
     * 
     * @since 1.0
     * @author awojtowi
     * @throws IOException 
     */
    public static void deleteGeneratedFiles() throws IOException {
    	final String classLoc = Classifier.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
    	final String dir = "../../../../src/main/resources/";
    	final String fileDir = (new File(classLoc + dir).getCanonicalFile()).toString();
    	final String ext = ".csv";
    	final String[] classFiles = new String[] {"general_literature","hardware",
    		"computer_systems_organization", "software","data",
    		"theory_of_computation","mathematics_of_computing", 
    		"information_systems", "computing_and_methodology", 
    		"computing_applications","computing_milieux"};
    	
    	File currFile;
    	for(int i=0; i<classFiles.length; i++) {
    		currFile = new File(fileDir + "/" + classFiles[i] + ext);
    		currFile.delete();
    	}
    }
    
/*    public String wekaClassification(Library lib, Document document) throws IOException
    {
    	lib.generateArff();
    	final String classLoc = Classify.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
    	final String dir = "../../../../src/main/resources/";
    	final String fileDir = (new File(classLoc + dir).getCanonicalFile()).toString();
    	final String ext = ".arff";
    	final String file = fileDir + "/library" + ext;
    	BufferedReader breader = new BufferedReader(new FileReader(file));
    	Instances m_Data = new Instances(breader);
    	m_Data.setClassIndex(m_Data.numAttributes()-1);

    	StringToWordVector m_Filter = new StringToWordVector();
    	Classifier m_Classifer = new J48();
    	
    	// Convert docSig to a usable string in Weka
    	HashMap<String, Integer> docSig = document.getDocSig();
    	Set<String> words = docSig.keySet();
    	String message = new String();
    	int count = 0;
    	for (String i: words)
    	{
    		if(count++ == words.size()-1)
    		{
    			message = message + i + " ";
    		}
    		else
    		{
    			message = message + i;
    		}
    	}    	
    	// Setup Classifier modules
	    try {
    		m_Filter.setInputFormat(m_Data);
	    	Instances filteredData = Filter.useFilter(m_Data, m_Filter);
	    	m_Classifer.buildClassifier(filteredData);
	    	Instances testset = m_Data.stringFreeStructure();
	    	
	    	//Convert docSig to a Instance for WEKA
	    	Instance instance = new DenseInstance(2);
	    	Attribute messageAtt = testset.attribute("document");
	    	instance.setValue(messageAtt, messageAtt.addStringValue(message));
	    	instance.setDataset(testset);
	    	
	    	m_Filter.input(instance);
	    	Instance filteredInstance = m_Filter.output();
		    double predicted = m_Classifer.classifyInstance(filteredInstance);
	    	return m_Data.classAttribute().value((int) predicted);
	    }
	    catch (Exception e)
	    {
	    	System.err.println("Classification Error");
	    	return null;
	    }
    }
*/ 
}  // End of Classifier.java