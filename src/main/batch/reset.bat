@echo OFF
CD ../resources

echo "----------------------------------------------------------"

ERASE library.txt
echo "Resetting library.txt..."
echo "a " >> library.txt
echo "0 " >> library.txt
echo "0 " >> library.txt
echo "0 " >> library.txt
echo "0 " >> library.txt
echo "0 " >> library.txt
echo "0 " >> library.txt
echo "0 " >> library.txt
echo "0 " >> library.txt
echo "0 " >> library.txt
echo "0 " >> library.txt
echo "0 " >> library.txt
echo "**/src/main/resources/library.txt was reset!"

ERASE throwOutWords.txt
echo "Resetting throwOutWords.txt..."
echo "this" >> throwOutWords.txt
echo "that" >> throwOutWords.txt
echo "a" >> throwOutWords.txt
echo "are" >> throwOutWords.txt
echo "was" >> throwOutWords.txt
echo "it" >> throwOutWords.txt
echo "no" >> throwOutWords.txt
echo "as" >> throwOutWords.txt
echo "**/src/main/resources/throwOutWords.txt was reset!"

echo "----------------------------------------------------------"
EXIT