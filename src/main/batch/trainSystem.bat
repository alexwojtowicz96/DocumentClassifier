@echo OFF
#!/bin/bash
chmod 755 reset.sh
./reset.sh

echo " "
echo "Training the system..."

trainDir="../data"

a="/A. General literature"
b="/B. Hardware"
c="/C. Computer systems organization"
d="/D. Software and its engineering"
e="/E. Data"
f="/F. Theory of computation"
g="/G. Mathematics of computing"
h="/H. Information systems"
i="/I. Computing and methodologies"
j="/J. Computing applications"
k="/K. Computing milieux"

cd "$trainDir/$a"
for FILE in ls *
do
  if [ "$FILE" != "ls" ]; then
    echo "TRAINING -> $FILE"
    java -jar ../../../../build/libs/Train-0.1.jar -ClassifyFile "$FILE" -ClassifyType "A"
  fi
done
cd "../$b"
for FILE in ls *
do
  if [ "$FILE" != "ls" ]; then
    echo "TRAINING -> $FILE"
    java -jar ../../../../build/libs/Train-0.1.jar -ClassifyFile "$FILE" -ClassifyType "B"
  fi
done
cd "../$c"
for FILE in ls *
do
  if [ "$FILE" != "ls" ]; then
    echo "TRAINING -> $FILE"
    java -jar ../../../../build/libs/Train-0.1.jar -ClassifyFile "$FILE" -ClassifyType "C"
  fi
done
cd "../$d"
for FILE in ls *
do
  if [ "$FILE" != "ls" ]; then
    echo "TRAINING -> $FILE"
    java -jar ../../../../build/libs/Train-0.1.jar -ClassifyFile "$FILE" -ClassifyType "D"
  fi
done
cd "../$e"
for FILE in ls *
do
  if [ "$FILE" != "ls" ]; then
    echo "TRAINING -> $FILE"
    java -jar ../../../../build/libs/Train-0.1.jar -ClassifyFile "$FILE" -ClassifyType "E"
  fi
done
cd "../$f"
for FILE in ls *
do
  if [ "$FILE" != "ls" ]; then
    echo "TRAINING -> $FILE"
    java -jar ../../../../build/libs/Train-0.1.jar -ClassifyFile "$FILE" -ClassifyType "F"
  fi
done
cd "../$g"
for FILE in ls *
do
  if [ "$FILE" != "ls" ]; then
    echo "TRAINING -> $FILE"
    java -jar ../../../../build/libs/Train-0.1.jar -ClassifyFile "$FILE" -ClassifyType "G"
  fi
done
cd "../$h"
for FILE in ls *
do
  if [ "$FILE" != "ls" ]; then
    echo "TRAINING -> $FILE"
    java -jar ../../../../build/libs/Train-0.1.jar -ClassifyFile "$FILE" -ClassifyType "H"
  fi
done
cd "../$i"
for FILE in ls *
do
  if [ "$FILE" != "ls" ]; then
    echo "TRAINING -> $FILE"
    java -jar ../../../../build/libs/Train-0.1.jar -ClassifyFile "$FILE" -ClassifyType "I"
  fi
done
cd "../$j"
for FILE in ls *
do
  if [ "$FILE" != "ls" ]; then
    echo "TRAINING -> $FILE"
    java -jar ../../../../build/libs/Train-0.1.jar -ClassifyFile "$FILE" -ClassifyType "J"
  fi
done
cd "../$k"
for FILE in ls *
do
  if [ "$FILE" != "ls" ]; then
    echo "TRAINING -> $FILE"
    java -jar ../../../../build/libs/Train-0.1.jar -ClassifyFile "$FILE" -ClassifyType "K"
  fi
done

echo "FINISHED"
echo " " 
EXIT
