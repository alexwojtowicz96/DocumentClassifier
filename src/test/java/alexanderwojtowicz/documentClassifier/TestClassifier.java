package alexanderwojtowicz.documentClassifier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.Ignore;
// For Debugging:
//import java.net.URL;
//import java.net.URLClassLoader;

public class TestClassifier {
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	//** Data Member Declarations. **//
	Library testLib;        // A test Library
	Classifier classifier;  // The classifier.
	
	Document docA;       // A Document that should be classified as: "General Literature".
	Document docB;       // A Document that should be classified as: "Hardware".
	Document docC;       // A Document that should be classified as: "Computer Systems Organization".
	Document docD;       // A Document that should be classified as: "Software".
	Document docE;       // A Document that should be classified as: "Data".
	Document docF;       // A Document that should be classified as: "Theory of Computation".
	Document docG;       // A Document that should be classified as: "Mathematics of Computing".
	Document docH;       // A Document that should be classified as: "Information Systems".
	Document docI;       // A Document that should be classified as: "Computing and Methodology".
	Document docJ;       // A Document that should be classified as: "Computer Applications".
	Document docK;       // A Document that should be classified as: "Computing Milieux".
	Document shortDoc;   // A small predictable document: **/Blue1/src/test/data/30+words.txt
	
	String testDir;                 // Directory: **/Blue1/src/main/data/
	String test2Dir;                // Directory: **/Blue1/src/test/data/
	String genDir;                  // Directory: **/Blue1/src/main/resources/
	String general_literature;      // Directory: /A.\ General\ literature/
	String hardware;                // Directory: /B.\ Hardware/
	String computer_systems_org;    // Directory: /C.\ Computer\ systems\ organization/
	String software;                // Directory: /D.\ Software\ and\ its\ engineering/
	String data;                    // Directory: /E.\ Data/
	String theory_of_comp;          // Directory: /F.\ Theory\ of\ computation/
	String mathematics_of_comp;     // Directory: /G.\ Mathematics\ of\ computing/
	String information_systems;     // Directory: /H.\ Information\ systems/
	String computing_and_method;    // Directory: /I.\ Computing\ and\ methodologies/
	String computing_apps;          // Directory: /J.\ Computing\ applications/
	String computing_milieux;       // Directory: /K.\ Computing\ milieux/
	
	File initLib;              // **/Blue1/src/test/data/initLib.txt
	File demoLib2;             // **/Blue1/src/test/data/demoLib2.txt
	File demoThrowOutWords1;   // **/Blue1/src/test/data/demoThrowOutWords1.txt
	File demoThrowOutWords2;   // **/Blue1/src/test/data/demoThrowOutWords2.txt
	
	@Before
	public void SetUp() throws Exception {
		//** Data Member Initializations. **//	
		String classLoc    = TestClassifier.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
		String tests       = "/../../../../src/main/data/"; 
		String tests2      = "/../../../../src/test/data/";
		String gen         = "/../../../../src/main/resources/";
		testDir            = (new File(classLoc + tests).getCanonicalFile()).toString();
		test2Dir           = (new File(classLoc + tests2).getCanonicalFile()).toString();
		genDir             = (new File(classLoc + gen).getCanonicalFile()).toString();
		demoLib2           = new File(test2Dir + "/demoLib2.txt");
		demoThrowOutWords1 = new File(test2Dir + "/demoThrowOutWords1.txt");
		demoThrowOutWords2 = new File(test2Dir + "/demoThrowOutWords2.txt");
		initLib            = new File(test2Dir + "/initLib.txt");
		testLib            = new Library();
		testLib.setLibraryFile(initLib);
		testLib.setThrowOutWordsFile(demoThrowOutWords1);
		testLib.readLibrary();
		testLib.readThrowOutWords();
		
		shortDoc = new Document(new File(test2Dir + "/30+words.txt"));
		
		general_literature   = (testDir + "/A. General literature/");
		hardware             = (testDir + "/B. Hardware/");
		computer_systems_org = (testDir + "/C. Computer systems organization/");
		software             = (testDir + "/D. Software and its engineering/");
		data                 = (testDir + "/E. Data/");
		theory_of_comp       = (testDir + "/F. Theory of computation/");
		mathematics_of_comp  = (testDir + "/G. Mathematics of computing/");
		information_systems  = (testDir + "/H. Information systems/");
		computing_and_method = (testDir + "/I. Computing and methodologies/");
		computing_apps       = (testDir + "/J. Computing applications/");
		computing_milieux    = (testDir + "/K. Computing milieux/");
		
		//classifier = new Classify();
	}	
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@After
	public void reset() throws FileNotFoundException {
		LinkedList<String> toRevert = new LinkedList<String>();
		toRevert.add("a ");
		for(int i=0; i<11; i++) {
			toRevert.add("0 ");
		}
		PrintWriter writer = new PrintWriter(initLib);
		writer.flush();
		for(int i=0; i<toRevert.size(); i++) {
			writer.println(toRevert.get(i));
		}
		writer.close();
		toRevert.clear();
		writer = new PrintWriter(demoThrowOutWords1);
		writer.println("this");
		writer.println("is");
		writer.println("a");
		writer.println("test");
		writer.println("and");
		writer.println("these");
		writer.println("represent");
		writer.println("words");
		writer.println("to");
		writer.println("prune");
		writer.close();
		
		writer = new PrintWriter(demoThrowOutWords2);
		writer.println("30");
		writer.println("thirty");
		writer.close();
	}
//*** ---------------------------------------------------- TEST CASES ---------------------------------------------------- ***//
	@Ignore("To save building time, Test_ClassifyDocument() is being ignored")
	@Test
	public void Test_ClassifyDocument() throws IOException {
		//fail("*** Classify::classifyDocument is not yet implemented! ***");
		testLib = new Library();
		// Loading the hash maps takes a long time...
		docA = new Document(new File (general_literature   + "/p98-kurvinen.pdf"));
		docB = new Document(new File (hardware             + "/p11-hardin.pdf"));
		docC = new Document(new File (computer_systems_org + "/p12-hu.pdf"));
		docD = new Document(new File (software             + "/p1-sheard.pdf"));
		docE = new Document(new File (data                 + "/p18-cui.pdf"));
		docF = new Document(new File (theory_of_comp       + "/p62-bouajjani.pdf"));
		docG = new Document(new File (mathematics_of_comp  + "/p25-zha.pdf"));
		docH = new Document(new File (information_systems  + "/p155-gentry.pdf"));
		docI = new Document(new File (computing_and_method + "/p10-wren.pdf"));
		docJ = new Document(new File (computing_apps       + "/p11-walker.pdf"));
		docK = new Document(new File (computing_milieux    + "/a19-cooper.pdf"));
		
		int correct = 0; // Increment if expected classification = resulting classification.
		final int neededToPass = 3;  // How many do we want to be classified correctly for this test to pass? Adjust accordingly.
		LinkedList<Document> toClassify = new LinkedList<Document>();  // Pre-classification
		LinkedList<Document> classified = new LinkedList<Document>();  // Post-classification
		String[] classOptions = new String[11];
		classOptions[0]  = "General Literature";
		classOptions[1]  = "Hardware";
		classOptions[2]  = "Computer Systems Organization";
		classOptions[3]  = "Software";
		classOptions[4]  = "Data";
		classOptions[5]  = "Theory of Computation";
		classOptions[6]  = "Mathematics of Computing";
		classOptions[7]  = "Information Systems";
		classOptions[8]  = "Computing and Methodology";
		classOptions[9]  = "Computing Applications";
		classOptions[10] = "Computing Milieux";
		
		Document doc = new Document();
		toClassify.add(docA);
		toClassify.add(docB);
		toClassify.add(docC);
		toClassify.add(docD);
		toClassify.add(docE);
		toClassify.add(docF);
		toClassify.add(docG);
		toClassify.add(docH);
		toClassify.add(docI);
		toClassify.add(docJ);
		toClassify.add(docK);	
		
		// These files should all yield valid documents
		for(int i=0; i<toClassify.size(); i++) {
			doc = toClassify.get(i);
			if(doc.isValid()) {
				System.out.println(doc.getPath().toString() + " : \n***VALID");
			}
			else {
				System.out.println(doc.getPath().toString() + " : \n***INVALID");
			}
		}
		for(int i=0; i<toClassify.size(); i++) {
			assertTrue(toClassify.get(i).isValid());
		}
		
		System.out.println();
		System.out.println("****************** Test_ClassifyDocument() ******************");
		for(int i=0; i<toClassify.size(); i++) {
			System.out.println("PATH: " + toClassify.get(i).getPath().toString());
			System.out.println("EXPECTED  :  " + classOptions[i]);
			//classifiedDoc = classifier.classifyDocument(toClassify.get(i));
			doc = Classifier.classifyDocument(toClassify.get(i), testLib);
			System.out.println("RESULT    :  " + doc.getClassification());
			classified.add(doc);
			System.out.println();
			
			if(doc.getClassification().equals(classOptions[i])) {
				correct++;
			}
		}
		System.out.println("*************************************************************");
		System.out.println("CORRECT  :  " + correct);
		System.out.println("NEEDED   :  " + neededToPass);
		if(correct < neededToPass) {
			System.out.println("*** Fail ***");
			fail();
		}
		else {
			System.out.println("*** Success ***");
		}
	}	
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_PrepareDocument() throws IOException {
		assertTrue(shortDoc.isValid());
		testLib.setLibraryFile(demoLib2);
		testLib.readLibrary();
		assertNotNull(testLib);
		
		System.out.println("Test Classify::prepareDocument() *************************************\n");
		System.out.println("Before prepareDocument() ->");
		
		// I am adding some words that will stay and some words that will be pruned.
		shortDoc.getDocSig().put("jelly", 778);     // stays
		shortDoc.getDocSig().put("butter", 9);      // stays
		shortDoc.getDocSig().put("bean", 5);        // stays
		shortDoc.getDocSig().put("santa", 4);       // goes (<5 instances)
		shortDoc.getDocSig().put("trampoline", 0);  // goes (<5 instances)
		shortDoc.getDocSig().put("prune", 3);       // goes (<5 instances and ignored)
		shortDoc.getDocSig().put("represent", 80);  // goes (ignored)
		shortDoc.getDocSig().put("dinner", 2);      // goes (<5 instances)
		shortDoc.getDocSig().put("lunch", 1);       // goes (<5 instances)
		shortDoc.getDocSig().put("breakfast", 0);   // goes (<5 instances)
		shortDoc.getDocSig().put("cucumber", 600);  // stays (already in Library)
		shortDoc.getDocSig().put("egg", 4);         // stays (already in Library but value should become 0)
		
		shortDoc.display();
		shortDoc = Classifier.prepareDocument(shortDoc, testLib);
		System.out.println("\nAfter prepareDocument() ->");
		shortDoc.display();
		System.out.println();
		
		HashMap<String, Integer> toCompare = new HashMap<String, Integer>();
		Set<String> libWords = testLib.getClassMap().get("General Literature").keySet();
		for(String i : libWords) {
			toCompare.put(i, 0);
		}
		toCompare.put("cucumber", 600);
		toCompare.put("jelly", 778);
		toCompare.put("butter", 9);
		toCompare.put("bean", 5);
		toCompare.put("sentence", 5);
		toCompare.put("normal", 5);
		toCompare.put("no", 5);
		toCompare.put("including", 5);
		toCompare.put("added", 5);
		toCompare.put("in", 10);
		toCompare.put("strange", 5);
		toCompare.put("the", 5);
		toCompare.put("characters", 5);
		toCompare.put("number", 5);
		toCompare.put("total", 5);
		toCompare.put("file", 5);
		toCompare.put("are", 10);
		toCompare.put("moderation", 5);
		toCompare.put("there", 10);
		toCompare.put("of", 5);
		toCompare.put("punctuation", 5);
		toCompare.put("30", 5);
		toCompare.put("thirty", 5);
		
		//System.out.println(toCompare.size());
		//System.out.println(shortDoc.getDocSig().size());
		assertThat(toCompare.size(), is (shortDoc.getDocSig().size()));
		Set<String> docKeys = shortDoc.getDocSig().keySet();
		Set<String> compKeys = toCompare.keySet();
		for(String i : docKeys) {
			assertTrue(toCompare.containsKey(i));
			assertThat(shortDoc.getDocSig().get(i), is (toCompare.get(i)));
		}
		for(String i : compKeys) {
			assertTrue(shortDoc.getDocSig().containsKey(i));
			assertThat(toCompare.get(i), is (shortDoc.getDocSig().get(i)));
		}
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_PrepareLibrary() throws IOException {
		assertTrue(shortDoc.isValid());
		testLib.setLibraryFile(demoLib2);
		testLib.readLibrary();
		assertNotNull(testLib);
		
		shortDoc.getDocSig().put("jelly", 778);     // added...0's for all classes
		shortDoc.getDocSig().put("butter", 9);      // added...0's for all classes
		shortDoc.getDocSig().put("bean", 5);        // added...0's for all classes
		shortDoc.getDocSig().put("santa", 4);       // not added
		shortDoc.getDocSig().put("trampoline", 0);  // not added
		shortDoc.getDocSig().put("prune", 3);       // not added
		shortDoc.getDocSig().put("represent", 80);  // not added
		shortDoc.getDocSig().put("dinner", 2);      // not added
		shortDoc.getDocSig().put("lunch", 1);       // not added
		shortDoc.getDocSig().put("breakfast", 0);   // not added
		shortDoc.getDocSig().put("cucumber", 600);  // already there...0's for all classes
		shortDoc.getDocSig().put("egg", 4);         // already there...0's for all classes
		
		System.out.println("Test Classify::prepareLibrary() *************************************\n");
		System.out.println("Before prepareLibrary() ->");
		testLib.display();
		shortDoc = Classifier.prepareDocument(shortDoc, testLib);
		testLib = Classifier.prepareLibrary(shortDoc, testLib);
		System.out.println("\nAfter prepareLibrary() ->");
		testLib.display();
		
		Set<String> classes = testLib.getClassMap().keySet();
		for(String c : classes) {
			assertThat(testLib.getClassMap().get(c).size(), is (shortDoc.getDocSig().size()));
		}
		// Use the original libraryFile and add to that for comparison...
		Library newLib = new Library();
		newLib.setLibraryFile(demoLib2);
		newLib.readLibrary();
		Set<String> docWords = shortDoc.getDocSig().keySet();
		Set<String> origWords = newLib.getClassMap().get("General Literature").keySet();
		for(String d : docWords) {
			if(!origWords.contains(d)) {
				if(!newLib.getClassMap().get("General Literature").containsKey(d)) {
					for(String c : classes) {
						newLib.getClassMap().get(c).put(d, 0);
					}
				}
			}
		}
		//newLib.display();
		Set<String> compWords = newLib.getClassMap().get("General Literature").keySet();
		for(String c : classes) {
			for(String w : compWords) {
				assertTrue(testLib.getClassMap().get(c).containsKey(w));
				assertTrue(newLib.getClassMap().get(c).get(w) == testLib.getClassMap().get(c).get(w));
			}
		}
		compWords = testLib.getClassMap().get("General Literature").keySet();
		for(String c : classes) {
			for(String w : compWords) {
				assertTrue(newLib.getClassMap().get(c).containsKey(w));
				assertTrue(testLib.getClassMap().get(c).get(w) == newLib.getClassMap().get(c).get(w));
			}
		}
	}	
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_Generate() throws IOException {
		assertNotNull(testLib);
		assertNotNull(shortDoc);
		assertTrue(shortDoc.isValid());
		shortDoc = Classifier.prepareDocument(shortDoc, testLib);
		testLib = Classifier.prepareLibrary(shortDoc, testLib);
		
		Classifier.deleteGeneratedFiles();
		final String ext = ".csv";
		File currFile;
		for(int i=0; i<testLib.getClassifications().length; i++) {
			String fName = testLib.getClassifications()[i].toLowerCase().replace(' ', '_');
			currFile = new File(genDir + "/" + fName + ext);
			assertFalse(currFile.exists());
		}
		
		Classifier.generate(testLib, shortDoc);
		for(int i=0; i<testLib.getClassifications().length; i++) {
			String fName = testLib.getClassifications()[i].toLowerCase().replace(' ', '_');
			currFile = new File(genDir + "/" + fName + ext);
			assertTrue(currFile.exists());
		}
		
		Set<String> classes = testLib.getClassMap().keySet();
		Set<String> libKeys = testLib.getClassMap().get("General Literature").keySet();
		Set<String> docKeys = shortDoc.getDocSig().keySet();
		for(String c : classes) {
			assertTrue(testLib.getClassMap().get(c).size() == libKeys.size());
			assertTrue(shortDoc.getDocSig().size() == libKeys.size());
			for(String w : libKeys) {
				assertTrue(docKeys.contains(w));
			}
			for(String w : docKeys) {
				assertTrue(libKeys.contains(w));
			}
		}
		// Checking the file contents is unnecessary and cannot be guaranteed since indexing may not be consistent.
		// As long as the data structures are correct, the generated files are correct.
		// These checks are done elsewhere: TestLibrary::Test_PrepareDocument() and TestLibrary::Test_PrepareLibrary().
		Classifier.deleteGeneratedFiles();
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_DeleteGeneratedFiles() throws IOException {
		assertNotNull(testLib);
		assertNotNull(shortDoc);
		assertTrue(shortDoc.isValid());
		shortDoc = Classifier.prepareDocument(shortDoc, testLib);
		testLib = Classifier.prepareLibrary(shortDoc, testLib);
		
		Classifier.deleteGeneratedFiles();
		final String ext = ".csv";
		File currFile;
		for(int i=0; i<testLib.getClassifications().length; i++) {
			String fName = testLib.getClassifications()[i].toLowerCase().replace(' ', '_');
			currFile = new File(genDir + "/" + fName + ext);
			assertFalse(currFile.exists());
		}
		
		Classifier.generate(testLib, shortDoc);
		for(int i=0; i<testLib.getClassifications().length; i++) {
			String fName = testLib.getClassifications()[i].toLowerCase().replace(' ', '_');
			currFile = new File(genDir + "/" + fName + ext);
			assertTrue(currFile.exists());
		}
		
		Classifier.deleteGeneratedFiles();
		for(int i=0; i<testLib.getClassifications().length; i++) {
			String fName = testLib.getClassifications()[i].toLowerCase().replace(' ', '_');
			currFile = new File(genDir + "/" + fName + ext);
			assertFalse(currFile.exists());
		}
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	
/*	@Test
	Public void Test_wekaClassification() throws IOException {
		testLib = new Library();
		// Loading the hash maps takes a long time...
		docA = new Document(new File (general_literature   + "/p98-kurvinen.pdf"));
		docB = new Document(new File (hardware             + "/p11-hardin.pdf"));
		docC = new Document(new File (computer_systems_org + "/p12-hu.pdf"));
		docD = new Document(new File (software             + "/p1-sheard.pdf"));
		docE = new Document(new File (data                 + "/p18-cui.pdf"));
		docF = new Document(new File (theory_of_comp       + "/p62-bouajjani.pdf"));
		docG = new Document(new File (mathematics_of_comp  + "/p25-zha.pdf"));
		docH = new Document(new File (information_systems  + "/p155-gentry.pdf"));
		docI = new Document(new File (computing_and_method + "/p10-wren.pdf"));
		docJ = new Document(new File (computing_apps       + "/p11-walker.pdf"));
		docK = new Document(new File (computing_milieux    + "/a19-cooper.pdf"));
		
		int correct = 0; // Increment if expected classification = resulting classification.
		final int neededToPass = 3;  // How many do we want to be classified correctly for this test to pass? Adjust accordingly.
		LinkedList<Document> toClassify = new LinkedList<Document>();  // Pre-classification
		LinkedList<Document> classified = new LinkedList<Document>();  // Post-classification
		String[] classOptions = new String[11];
		classOptions[0]  = "General Literature";
		classOptions[1]  = "Hardware";
		classOptions[2]  = "Computer Systems Organization";
		classOptions[3]  = "Software";
		classOptions[4]  = "Data";
		classOptions[5]  = "Theory of Computation";
		classOptions[6]  = "Mathematics of Computing";
		classOptions[7]  = "Information Systems";
		classOptions[8]  = "Computing and Methodology";
		classOptions[9]  = "Computing Applications";
		classOptions[10] = "Computing Milieux";
		
		Document doc = new Document();
		toClassify.add(docA);
		toClassify.add(docB);
		toClassify.add(docC);
		toClassify.add(docD);
		toClassify.add(docE);
		toClassify.add(docF);
		toClassify.add(docG);
		toClassify.add(docH);
		toClassify.add(docI);
		toClassify.add(docJ);
		toClassify.add(docK);
		
		for (int i =0; i <toClassify.size(); i++)
		{
			String document = testLib.wekaClassification(testLib, i);
			if(document == classOptions[i])
			{
				correct++;
			}
		}
		if (correct > neededToPass)
		{
			assert(true);
		}
		else
		{
			assert(false);
		}
	}
	
*/
//*** -------------------------------------------------------------------------------------------------------------------- ***//

} // End of TestClassifier.java