package alexanderwojtowicz.documentClassifier;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

//import java.util.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

//import edu.odu.cs.cs350.*;

// For this class to pass all tests, DocumentSignature must have passed all tests.
// This is a test class for Document.java.
// This test assumes that there are files ready to read in Blue1/src/test/data.
// This class will also test the validity of a document (make sure it is of a
// supported file format between 1-50 pages).

public class TestDocument {
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	//** Data Member Declarations. **//
	Document doc1;                         // Default Document.
	Document doc2;                         // Document w/ path (.txt) (no classification).
	Document doc3;                         // Document w/ path (.pdf) (no classification).
	Document doc4;                         // Document w/ path (.txt) AND known classification.
	Document doc5;                         // Document w/ path (.pdf) AND known classification.
	Document testDoc;                      // Document w/ every attribute known (w/ .txt filePath).
	File textPath;                         // Path to a .txt file. (file type IS supported)
	File pdfPath;                          // Path to a .pdf file. (file type IS supported)
	File otherPath1;                       // Path to a .rtf file. (file type not supported)
	File otherPath2;                       // Path to a .docx file. (file type not supported)
	File invalid;                          // Path to non-existent file.
	File emptyTxt;                         // ASCII Text file (one page) w/ no text.
	File emptyPdf;                         // PDF file (one page) w/ no text.
	String class1;                         // ACM Classification (String type).
	String class2;                         // Another ACM Classification (String type).
	String testDir;                        // Directory where test files are located.
	HashMap<String, Integer> emptyMap;     // An empty has map 
	HashMap<String, Integer> map1;         // Hash map for 30words.txt and 30words.pdf
	
	@Before
	public void SetUp() throws Exception {
		//** Data Member Initializations. **//
		// FROM: https://stackoverflow.com/questions/11747833/getting-filesystem-path-of-class-being-executed
		// Location of edu.odu.cs.cs350.TestDocument
		String classLoc = TestDocument.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
		//** **//
		
		String tests  = "/../../../../src/test/data/";  
		testDir       = new File(classLoc + tests).toString();
		class1        = "Software";
		class2        = "General Literature";
		textPath      = new File(testDir + "/test1.txt");
		pdfPath       = new File(testDir + "/test4.pdf");
		otherPath1    = new File(testDir + "/test2.rtf");
		otherPath2    = new File(testDir + "/test3.docx");
		invalid       = new File(testDir + "/asdddj.txt");
		emptyTxt      = new File(testDir + "/empty.txt");
		emptyPdf      = new File(testDir + "/empty.pdf");
		emptyMap      = new HashMap<String, Integer>();
		map1          = new HashMap<String, Integer>();
		
		doc1    = new Document();
		doc2    = new Document(textPath);
		doc3    = new Document(pdfPath);
		doc4    = new Document(textPath, class1);
		doc5    = new Document(pdfPath, class2);
		testDoc = new Document(textPath, 10000, 20, "ASCII Text", class1);
		
		map1.put("sentence", 1);
		map1.put("normal", 1);
		map1.put("no", 1);
		map1.put("a", 2);
		map1.put("including", 1);
		map1.put("added", 1);
		map1.put("in", 2);
		map1.put("words", 1);
		map1.put("this", 2);
		map1.put("is", 2);
		map1.put("strange", 1);
		map1.put("the", 1);
		map1.put("characters", 1);
		map1.put("number", 1);
		map1.put("total", 1);
		map1.put("file", 1);
		map1.put("are", 2);
		map1.put("and", 1);
		map1.put("moderation", 1);
		map1.put("there", 2);
		map1.put("of", 1);
		map1.put("punctuation", 1);
		map1.put("30", 1);
		map1.put("thirty", 1);
	}
//*** -------------------------------------------------- * TEST CASES * -------------------------------------------------- ***//
	@Test
	/*
	 * Test the default constructor. This should never be invoked.
	 */
	public void Test_Constructor1() {
		String classLoc = Document.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
		String tests = "../../../../src/test/data/";
		assertThat(doc1.getPath().toString(), is (classLoc + tests + "nothing.txt"));
		assertThat(doc1.getWordCount(), is (0));
		assertThat(doc1.getDocLength(), is (0));
		assertTrue(doc1.getClassification().equals("noClass"));
		assertTrue(doc1.getFileType().equals("noType"));
		assertTrue(doc1.getDocSig().equals(emptyMap));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	/*
	 * Test constructor where we have a known file path (for classifying).
	 */
	public void Test_Constructor2() throws IOException {
		assertTrue(doc2.getPath().exists());
		assertTrue(doc3.getPath().exists());
		
		testDoc = new Document(otherPath1);
		assertTrue(testDoc.getPath().exists());
		
		testDoc = new Document(new File(testDir + "/30words.txt"));
		assertTrue(testDoc.getPath().equals(new File(testDir + "/30words.txt").getCanonicalFile()));
		assertTrue(testDoc.getFileType().equals("ASCII Text"));
		assertThat(testDoc.getDocLength(), is (1));
		assertThat(testDoc.getWordCount(), is (30));
		assertTrue(testDoc.getClassification().equals("noClass"));
		Set<String> map1_keys = map1.keySet();
		Set<String> docSig_keys = testDoc.getDocSig().keySet();
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		
		// If they are the same size, then check to see if the key : value pairs are the same.
		for(String i : map1_keys) {
			assertTrue(testDoc.getDocSig().containsKey(i));
			assertThat(map1.get(i), is (testDoc.getDocSig().get(i)));
		}
		map1.put("there", 3);
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		assertFalse(map1.get("there").toString().equals(testDoc.getDocSig().get("there").toString()));
		
		testDoc = new Document(new File(testDir + "/30words.pdf"));
		assertTrue(testDoc.getFileType().equals("PDF"));
		assertThat(testDoc.getDocLength(), is (1));
		assertThat(testDoc.getWordCount(), is (30));
		assertTrue(testDoc.getClassification().equals("noClass"));
		assertTrue(testDoc.getPath().equals(new File(testDir + "/30words.pdf").getCanonicalFile()));
		map1.remove("there");
		map1.put("there", 2);
		map1_keys = map1.keySet();
		docSig_keys = testDoc.getDocSig().keySet();
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		
		for(String i : map1_keys) {
			assertTrue(testDoc.getDocSig().containsKey(i));
			assertThat(map1.get(i), is (testDoc.getDocSig().get(i)));
		}
		map1.put("there", 3);
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		assertFalse(map1.get("there").toString().equals(testDoc.getDocSig().get("there").toString()));	
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	/*
	 * Test constructor where we have known file path and known classification.
	 * Used for supplying documents to train the system.
	 */
	public void Test_Constructor3() throws IOException {
		assertTrue(doc4.getPath().exists());
		assertTrue(doc5.getPath().exists());
		assertTrue(doc4.getClassification().equals("Software"));
		assertTrue(doc5.getClassification().equals("General Literature"));
		
		testDoc = new Document(otherPath1);
		assertTrue(testDoc.getPath().exists());
		
		testDoc = new Document(new File(testDir + "/30words.txt"), "noClass");
		assertTrue(testDoc.getPath().equals(new File(testDir + "/30words.txt").getCanonicalFile()));
		assertTrue(testDoc.getFileType().equals("ASCII Text"));
		assertThat(testDoc.getDocLength(), is (1));
		assertThat(testDoc.getWordCount(), is (30));
		assertTrue(testDoc.getClassification().equals("noClass"));
		Set<String> map1_keys = map1.keySet();
		Set<String> docSig_keys = testDoc.getDocSig().keySet();
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		
		// If they are the same size, then check to see if the key : value pairs are the same.
		for(String i : map1_keys) {
			assertTrue(testDoc.getDocSig().containsKey(i));
			assertThat(map1.get(i), is (testDoc.getDocSig().get(i)));
		}
		map1.put("there", 3);
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		assertFalse(map1.get("there").toString().equals(testDoc.getDocSig().get("there").toString()));
		
		testDoc = new Document(new File(testDir + "/30words.pdf"), "General Literature");
		assertTrue(testDoc.getFileType().equals("PDF"));
		assertThat(testDoc.getDocLength(), is (1));
		assertThat(testDoc.getWordCount(), is (30));
		assertTrue(testDoc.getClassification().equals("General Literature"));
		assertTrue(testDoc.getPath().equals(new File(testDir + "/30words.pdf").getCanonicalFile()));
		map1.remove("there");
		map1.put("there", 2);
		map1_keys = map1.keySet();
		docSig_keys = testDoc.getDocSig().keySet();
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		
		for(String i : map1_keys) {
			assertTrue(testDoc.getDocSig().containsKey(i));
			assertThat(map1.get(i), is (testDoc.getDocSig().get(i)));
		}
		map1.put("there", 3);
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		assertFalse(map1.get("there").toString().equals(testDoc.getDocSig().get("there").toString()));	
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	/*
	 * Test constructor where we have all attributes known (except docSignature).
	 * Used for testing purposes only.
	 */
	public void Test_Constructor4() throws IOException {
		assertNotNull(testDoc);
		assertThat(textPath.getCanonicalPath().toString(), is (testDoc.getPath().toString()));
		assertThat(testDoc.getWordCount(), is (10000));
		assertThat(testDoc.getDocLength(), is (20));
		assertThat(testDoc.getFileType(), is ("ASCII Text"));
		assertThat(testDoc.getClassification(), is (class1));
		assertTrue(testDoc.getDocSig().equals(emptyMap));
		
		testDoc = new Document(pdfPath, 10000, 20, "PDF", class2);
		assertNotNull(testDoc);
		assertThat(pdfPath.getCanonicalPath().toString(), is (testDoc.getPath().toString()));
		assertThat(testDoc.getWordCount(), is (10000));
		assertThat(testDoc.getDocLength(), is (20));
		assertThat(testDoc.getFileType(), is ("PDF"));
		assertThat(testDoc.getClassification(), is (class2));
		assertTrue(testDoc.getDocSig().equals(emptyMap));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	/*
	 * The Test_Set*() tests are only valid if ALL Test_Constructor*() tests pass.
	 * A lot of other tests in here depend on this one to be working.
	 */
	@Test  
	public void Test_SetPath() throws IOException {
		testDoc.setPath(textPath);
		assertTrue(testDoc.getPath().toString().equals(textPath.getCanonicalFile().toString()));
		testDoc.setPath(pdfPath);
		assertTrue(testDoc.getPath().toString().equals(pdfPath.getCanonicalFile().toString()));

		testDoc.setPath(otherPath1);
		assertTrue(testDoc.getPath().toString().equals(otherPath1.getCanonicalFile().toString()));
		testDoc.setPath(invalid);
		assertTrue(testDoc.getPath().toString().equals(otherPath1.getCanonicalFile().toString()));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test 
	public void Test_SetWordCount() {
		testDoc.setWordCount(90);
		assertThat(testDoc.getWordCount(), is (90));
		assertThat(testDoc.getDocLength(), is (20));
		assertThat(testDoc.getFileType(), is ("ASCII Text"));
		assertThat(testDoc.getClassification(), is (class1));
		
		testDoc.setWordCount(-1);
		assertThat(testDoc.getWordCount(), is (90));
		assertThat(testDoc.getDocLength(), is (20));
		assertThat(testDoc.getFileType(), is ("ASCII Text"));
		assertThat(testDoc.getClassification(), is (class1));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_SetDocLength() {	
		testDoc.setDocLength(1);
		assertThat(testDoc.getDocLength(), is (1));
		testDoc.setDocLength(-1);
		assertThat(testDoc.getDocLength(), is (1));
		testDoc.setDocLength(51);
		assertThat(testDoc.getDocLength(), is (1));
		testDoc.setDocLength(5);
		assertThat(testDoc.getDocLength(), is (5));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test 
	public void Test_SetFileType() {		
		testDoc.setFileType("ASCII Text");
		assertTrue(testDoc.getFileType().equals("ASCII Text"));
		testDoc.setFileType("PDF");
		assertTrue(testDoc.getFileType().equals("PDF"));
		testDoc.setFileType("Jibberish");
		assertTrue(testDoc.getFileType().equals("PDF"));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test  
	public void Test_SetClassification() {
		testDoc.setClassification("General Literature");
		assertTrue(testDoc.getClassification().equals("General Literature"));
		testDoc.setClassification("Hardware");
		assertTrue(testDoc.getClassification().equals("Hardware"));
		testDoc.setClassification("Computer Systems Organization");
		assertTrue(testDoc.getClassification().equals("Computer Systems Organization"));
		testDoc.setClassification("Software");
		assertTrue(testDoc.getClassification().equals("Software"));
		testDoc.setClassification("Data");
		assertTrue(testDoc.getClassification().equals("Data"));
		testDoc.setClassification("Theory of Computation");
		assertTrue(testDoc.getClassification().equals("Theory of Computation"));
		testDoc.setClassification("Mathematics of Computing");
		assertTrue(testDoc.getClassification().equals("Mathematics of Computing"));
		testDoc.setClassification("Information Systems");
		assertTrue(testDoc.getClassification().equals("Information Systems"));
		testDoc.setClassification("Computing and Methodology");
		assertTrue(testDoc.getClassification().equals("Computing and Methodology"));
		testDoc.setClassification("Computing Applications");
		assertTrue(testDoc.getClassification().equals("Computing Applications"));
		testDoc.setClassification("Computing Milieux");
		assertTrue(testDoc.getClassification().equals("Computing Milieux"));
		testDoc.setClassification("Not a Classification");  // This will not set.
		assertTrue(testDoc.getClassification().equals("Computing Milieux"));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_SetDocSig() {
		assertTrue(testDoc.getDocSig().equals(emptyMap));
		testDoc.setDocSig(map1);
		
		Set<String> map1_keys = map1.keySet();
		Set<String> docSig_keys = testDoc.getDocSig().keySet();
		assertThat(map1_keys.size(), is (docSig_keys.size()));

		for(String i : map1_keys) {
			assertTrue(testDoc.getDocSig().containsKey(i));
			assertThat(map1.get(i), is (testDoc.getDocSig().get(i)));
		}
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_FindWordCount() throws IOException {
		File txtThirty = new File(testDir + "/30words.txt");
		File pdfThirty = new File(testDir + "/30words.pdf");

		testDoc = new Document();
		assertThat(testDoc.getWordCount(), is (0));
		
		testDoc = new Document(txtThirty);
		testDoc.findWordCount();
		assertThat(testDoc.getWordCount(), is (30));
		
		testDoc = new Document(pdfThirty);
		testDoc.findWordCount();
		assertThat(testDoc.getWordCount(), is (30));
		
		testDoc = new Document(emptyPdf);
		testDoc.findWordCount();
		assertThat(testDoc.getWordCount(), is (0));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_FindDocLength() throws IOException {
		File fiftyPages = new File(testDir + "/50blankpages.pdf");
		File fiftyOnePages = new File(testDir + "/51blankpages.pdf");
		
		doc1.findDocLength(); 
		assertThat(doc1.getDocLength(), is (0));
		
		doc1.setPath(fiftyPages);
		doc1.setFileType("PDF");
		doc1.findDocLength();
		assertThat(doc1.getDocLength(), is (50));
		
		doc1.setPath(fiftyOnePages);
		doc1.setFileType("PDF");
		doc1.findDocLength();  
		assertThat(doc1.getDocLength(), is (50));
		
		doc1.setPath(textPath);
		doc1.setFileType("ASCII Text");
		doc1.findDocLength();
		assertThat(doc1.getDocLength(), is (1));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_FindFileType() throws IOException {
		testDoc = new Document();
		testDoc.setPath(textPath);
		assertTrue(testDoc.getFileType().equals("noType"));
		testDoc.findFileType();
		assertTrue(testDoc.getFileType().equals("ASCII Text"));
		
		testDoc = new Document();
		testDoc.setPath(pdfPath);
		assertTrue(testDoc.getFileType().equals("noType"));
		testDoc.findFileType();
		assertTrue(testDoc.getFileType().equals("PDF"));
		
		testDoc = new Document();
		testDoc.setPath(otherPath2);
		assertTrue(testDoc.getFileType().equals("noType"));
		testDoc.findFileType();
		assertTrue(testDoc.getFileType().equals("noType"));
		
		testDoc = new Document();
		testDoc.setPath(invalid);
		assertTrue(testDoc.getFileType().equals("noType"));
		testDoc.findFileType();
		assertTrue(testDoc.getFileType().equals("noType"));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_Display() throws IOException {
		testDoc = new Document(new File(testDir + "/30words.pdf"));
		assertTrue(testDoc.getPath().toString().equals(new File(testDir + "/30words.pdf").getCanonicalFile().toString()));
		assertTrue(testDoc.getClassification().equals("noClass"));
		assertThat(testDoc.getWordCount(), is (30));
		assertThat(testDoc.getDocLength(), is (1));
		Set<String> map1_keys = map1.keySet();
		Set<String> docSig_keys = testDoc.getDocSig().keySet();
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		
		for(String i : map1_keys) {
			assertTrue(testDoc.getDocSig().containsKey(i));
			assertThat(map1.get(i), is (testDoc.getDocSig().get(i)));
		}
		
		testDoc.display();
		
		// We don't want any side effects.
		testDoc = new Document(new File(testDir + "/30words.pdf"));
		assertTrue(testDoc.getPath().toString().equals(new File(testDir + "/30words.pdf").getCanonicalFile().toString()));
		assertTrue(testDoc.getClassification().equals("noClass"));
		assertThat(testDoc.getWordCount(), is (30));
		assertThat(testDoc.getDocLength(), is (1));
		map1_keys = map1.keySet();
		docSig_keys = testDoc.getDocSig().keySet();
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		
		for(String i : map1_keys) {
			assertTrue(testDoc.getDocSig().containsKey(i));
			assertThat(map1.get(i), is (testDoc.getDocSig().get(i)));
		}
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_IsValid() throws IOException {
		assertTrue(doc2.isValid());
		assertTrue(doc3.isValid());
		assertFalse(doc1.isValid());
		assertTrue(testDoc.isValid());
		assertTrue(doc4.isValid());
		assertTrue(doc5.isValid());
		
		testDoc = new Document(invalid);
		assertFalse(testDoc.isValid());
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_CheckFileExists() throws IOException {
		assertTrue(doc2.checkFileExists());
		assertTrue(doc3.checkFileExists());
		
		// An invalid file may still exist.
		assertTrue(doc4.checkFileExists());
		
		File invalidPath = new File(testDir + "/asdfjkl.pdf");
		testDoc = new Document(invalidPath, 10000, 20, "PDF", class1);
		assertFalse(testDoc.checkFileExists());
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_CheckFileSupport() throws IOException {
		assertTrue(doc2.checkFileSupport());    // .txt file
		assertTrue(doc3.checkFileSupport());    // .pdf file
		assertTrue(doc4.checkFileSupport());    // .txt file
		assertTrue(doc5.checkFileSupport());    // .txt file
		
		File wrongExt     = new File(testDir + "/test.jibberish");   
		File blankExt     = new File(testDir + "/test");               
		testDoc           = new Document(wrongExt, 10000, 20, "ASCII Text", class1);
		Document testDoc2 = new Document(blankExt, 10000, 20, "ASCII Text", class1);
		assertFalse(testDoc.checkFileSupport()); 
		assertFalse(testDoc2.checkFileSupport());
		
		testDoc = new Document(invalid);
		assertFalse(testDoc.checkFileSupport());
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_CheckFileLength() throws IOException {
		File fiftyPages    = new File(testDir + "/50blankpages.pdf");
		File fiftyOnePages = new File(testDir + "/51blankpages.pdf");
		File blankText     = new File(testDir + "/empty.txt");
		File blankPdf      = new File(testDir + "/empty.pdf");
		
		testDoc = new Document(fiftyPages);
		assertTrue(testDoc.checkFileLength());
		testDoc = new Document(fiftyOnePages);
		assertFalse(testDoc.checkFileLength());
		testDoc = new Document(blankText);
		assertTrue(testDoc.checkFileLength());
		testDoc = new Document(blankPdf);
		assertTrue(testDoc.checkFileLength());
		
		assertTrue(doc2.checkFileLength());
		assertTrue(doc3.checkFileLength());
		assertTrue(doc4.checkFileLength());
		assertTrue(doc5.checkFileLength());
		
		testDoc = new Document(invalid);
		assertFalse(testDoc.checkFileLength());	
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_PrintError() throws IOException {
		testDoc = new Document(new File(testDir + "/30words.pdf"));
		assertTrue(testDoc.getPath().toString().equals(new File(testDir + "/30words.pdf").getCanonicalFile().toString()));
		assertTrue(testDoc.getClassification().equals("noClass"));
		assertThat(testDoc.getWordCount(), is (30));
		assertThat(testDoc.getDocLength(), is (1));
		Set<String> map1_keys = map1.keySet();
		Set<String> docSig_keys = testDoc.getDocSig().keySet();
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		
		for(String i : map1_keys) {
			assertTrue(testDoc.getDocSig().containsKey(i));
			assertThat(map1.get(i), is (testDoc.getDocSig().get(i)));
		}
		
		testDoc.printError("This is a method...", "This is an explanation...");
		
		// We don't want any side effects.
		testDoc = new Document(new File(testDir + "/30words.pdf"));
		assertTrue(testDoc.getPath().toString().equals(new File(testDir + "/30words.pdf").getCanonicalFile().toString()));
		assertTrue(testDoc.getClassification().equals("noClass"));
		assertThat(testDoc.getWordCount(), is (30));
		assertThat(testDoc.getDocLength(), is (1));
		map1_keys = map1.keySet();
		docSig_keys = testDoc.getDocSig().keySet();
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		
		for(String i : map1_keys) {
			assertTrue(testDoc.getDocSig().containsKey(i));
			assertThat(map1.get(i), is (testDoc.getDocSig().get(i)));
		}	
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_ConvertToAbsolutePath() throws IOException {
		testDoc = new Document();
		assertTrue((testDoc.convertToAbsolutePath(pdfPath).toString()).equals(pdfPath.getCanonicalFile().toString()));
		assertTrue((testDoc.convertToAbsolutePath(invalid).toString().equals(invalid.toString())));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_DisplayDocSig() throws IOException {
		testDoc = new Document(new File(testDir + "/30words.pdf"));
		assertTrue(testDoc.getPath().toString().equals(new File(testDir + "/30words.pdf").getCanonicalFile().toString()));
		assertTrue(testDoc.getClassification().equals("noClass"));
		assertThat(testDoc.getWordCount(), is (30));
		assertThat(testDoc.getDocLength(), is (1));
		Set<String> map1_keys = map1.keySet();
		Set<String> docSig_keys = testDoc.getDocSig().keySet();
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		
		for(String i : map1_keys) {
			assertTrue(testDoc.getDocSig().containsKey(i));
			assertThat(map1.get(i), is (testDoc.getDocSig().get(i)));
		}
		
		testDoc.displayDocSig();
		
		// We don't want any side effects.
		testDoc = new Document(new File(testDir + "/30words.pdf"));
		assertTrue(testDoc.getPath().toString().equals(new File(testDir + "/30words.pdf").getCanonicalFile().toString()));
		assertTrue(testDoc.getClassification().equals("noClass"));
		assertThat(testDoc.getWordCount(), is (30));
		assertThat(testDoc.getDocLength(), is (1));
		map1_keys = map1.keySet();
		docSig_keys = testDoc.getDocSig().keySet();
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		
		for(String i : map1_keys) {
			assertTrue(testDoc.getDocSig().containsKey(i));
			assertThat(map1.get(i), is (testDoc.getDocSig().get(i)));
		}	
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_CreateHashMap() throws IOException {
		File thirtyWords = new File(testDir + "/30words.pdf");
		
		testDoc = new Document();
		assertThat(testDoc.getDocSig(), is (emptyMap));
		testDoc = new Document(thirtyWords);
		testDoc.createHashMap();
		Set<String> map1_keys = map1.keySet();
		Set<String> docSig_keys = testDoc.getDocSig().keySet();
		System.out.println(map1_keys.size());
		System.out.println(docSig_keys.size());
		assertThat(map1_keys.size(), is (docSig_keys.size()));
		
		for(String i : map1_keys) {
			assertTrue(testDoc.getDocSig().containsKey(i));
			assertThat(map1.get(i), is (testDoc.getDocSig().get(i)));
		}
		
		File blankPdf = new File(testDir + "/empty.pdf");
		testDoc = new Document();
		testDoc.setPath(blankPdf);
		testDoc.setFileType("PDF");
		
		testDoc.createHashMap();
		assertThat(testDoc.getDocSig(), is (emptyMap));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	
} // End of TestDocument.java