package alexanderwojtowicz.documentClassifier;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.After;
import org.junit.Test;
//import org.junit.Ignore;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

public class TestTrainer {
//*** ------------------------------------------------------------------------------------------------------------------- ***//
		//** Data Member Declarations. **//
		Trainer trainer;                 // An instance of Train
		Library testLibrary;             // A library (not using default file paths)
		String testDir;                  // Directory: **/Blue1/src/test/data/
		Document doc1;                   // **/Blue1/src/test/data/30words.txt
		Document doc2;                   // **/Blue1/src/test/data/30words.pdf
		Document doc3;                   // **/Blue1/src/test/data/30+words.txt
		File demoThrowOutWords2;         // **/Blue1/src/test/data/demoThrowOutWords2.txt
		File demoLib1;                   // **/Blue1/src/test/data/demoLib1.txt
		File demoLib2;                   // **/Blue1/src/test/data/ReadLibraryTest.txt
		HashMap<String, Integer> map1;   // A known document signature for 30words.*
		HashMap<String, Integer> map2;   // Used for document signature comparisons
	
		@Before
		public void SetUp() throws IOException {
			//** Data Member Initializations. **//
			String classLoc    = TestTrainer.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
			String tests       = "/../../../../src/test/data/";
			testDir            = (classLoc + tests); 
			doc1               = new Document(new File(testDir + "/30words.txt").getCanonicalFile());
			doc2               = new Document(new File(testDir + "/30words.pdf").getCanonicalFile());
			doc3               = new Document(new File(testDir + "/30+words.txt").getCanonicalFile());
			demoThrowOutWords2 = new File(testDir + "/demoThrowOutWords2.txt").getCanonicalFile();
			PrintWriter writer = new PrintWriter(demoThrowOutWords2);
			writer.flush();
			writer.println("30");
			writer.println("thirty");
			writer.close();
			demoLib1           = new File(testDir + "/demoLib1.txt").getCanonicalFile();
			demoLib2           = new File(testDir + "/ReadLibraryTest.txt").getCanonicalFile();
			testLibrary        = new Library();
			map1               = new HashMap<String, Integer>();
			map2               = new HashMap<String, Integer>();
			
			testLibrary.setLibraryFile(demoLib1);
			testLibrary.setThrowOutWordsFile(demoThrowOutWords2);
			testLibrary.readLibrary();
			testLibrary.readThrowOutWords();
			Set<String> classKeys = testLibrary.getClassMap().keySet();
			Set<String> wordKeys;
			for(String i : classKeys) {
				wordKeys = testLibrary.getClassMap().get(i).keySet();
				System.out.print(i + " --> | ");
				for(String k : wordKeys) {
					System.out.print(k + " " + testLibrary.getClassMap().get(i).get(k) + " | ");
				}
				System.out.println();
			}
			System.out.println();
			for(int i=0; i<testLibrary.getThrowOutWords().size(); i++) {
				System.out.println(testLibrary.getThrowOutWords().get(i).toString());
			}
			System.out.println();
			
			map1.put("sentence", 1);
			map1.put("normal", 1);
			map1.put("no", 1);
			map1.put("a", 2);
			map1.put("including", 1);
			map1.put("added", 1);
			map1.put("in", 2);
			map1.put("words", 1);
			map1.put("this", 2);
			map1.put("is", 2);
			map1.put("strange", 1);
			map1.put("the", 1);
			map1.put("characters", 1);
			map1.put("number", 1);
			map1.put("total", 1);
			map1.put("file", 1);
			map1.put("are", 2);
			map1.put("and", 1);
			map1.put("moderation", 1);
			map1.put("there", 2);
			map1.put("of", 1);
			map1.put("punctuation", 1);
			map1.put("30", 1);
			map1.put("thirty", 1);
			
			map2.put("sentence", 1);
			map2.put("normal", 1);
			map2.put("no", 1);
			map2.put("a", 2);
			map2.put("including", 1);
			map2.put("added", 1);
			map2.put("in", 2);
			map2.put("words", 1);
			map2.put("this", 2);
			map2.put("is", 2);
			map2.put("strange", 1);
			map2.put("the", 1);
			map2.put("characters", 1);
			map2.put("number", 1);
			map2.put("total", 1);
			map2.put("file", 1);
			map2.put("are", 2);
			map2.put("and", 1);
			map2.put("moderation", 1);
			map2.put("there", 2);
			map2.put("of", 1);
			map2.put("punctuation", 1);	
		}	
//*** ------------------------------------------------------------------------------------------------------------------- ***//	
		@After 
		public void resetThrowOutWords() throws FileNotFoundException {
			PrintWriter writer = new PrintWriter(demoThrowOutWords2);
			writer.flush();
			writer.println("30");
			writer.println("thirty");
			writer.close();
		}	
//*** ------------------------------------------------------------------------------------------------------------------- ***//
	@Test // This still needs a case where things are to be pruned... WIP
	public void Test_Train() throws IOException {
		assertNotNull(testLibrary);
		assertTrue(doc3.isValid());
		assertTrue(doc2.isValid());
		doc3.setClassification("General Literature");
		doc2.setClassification("Computing Milieux");
		
		testLibrary = Trainer.train(doc3, testLibrary);
		testLibrary = Trainer.train(doc2, testLibrary); // Not enough significant words...
		doc3.setClassification("Hardware");
		testLibrary = Trainer.train(doc3, testLibrary);
		doc3.setClassification("Software");
		testLibrary = Trainer.train(doc3, testLibrary);
		doc3.setClassification("Computing Applications");
		testLibrary = Trainer.train(doc3, testLibrary);
		doc3.setClassification("Data");
		testLibrary = Trainer.train(doc3, testLibrary);
		doc3.setClassification("Computer Systems Organization");
		testLibrary = Trainer.train(doc3, testLibrary);
		doc3.setClassification("Theory of Computation");
		testLibrary = Trainer.train(doc3, testLibrary);
		doc3.setClassification("Mathematics of Computing");
		testLibrary = Trainer.train(doc3, testLibrary);
		doc3.setClassification("Information Systems");
		testLibrary = Trainer.train(doc3, testLibrary);
		doc3.setClassification("Computing Milieux");
		testLibrary = Trainer.train(doc3, testLibrary);
		//doc3.setClassification("Computing and Methodology");
		//testLibrary = Trainer.train(doc3, testLibrary);
		
		Set<String> classKeys = testLibrary.getClassMap().keySet();
		Set<String> wordKeys;
		System.out.println("Testing : Train::train()");
		for(String i : classKeys) {
			wordKeys = testLibrary.getClassMap().get(i).keySet();
			String base = "--> | ";
			String buffer = " ";
			int numSpaces = (29 - i.length());
			for(int j=0; j<numSpaces; j++) {
				buffer = buffer + "-";
			}
			System.out.print(i + buffer + base);
			for(String k : wordKeys) {
				System.out.print(k + " " + testLibrary.getClassMap().get(i).get(k) + " | ");
			}
			System.out.println();
		}
		System.out.println();
		
		Set<String> allClasses = testLibrary.getClassMap().keySet();
		Set<String> allWords = testLibrary.getClassMap().get("General Literature").keySet();
		// All of the keys for every classification should be the same (only the values differ).
		for(String i : allClasses) {
			HashMap<String, Integer> currMap = testLibrary.getClassMap().get(i);
			for(String k : allWords) {
				assertTrue(currMap.containsKey(k));
			}
		}
		for(String i : allWords) {
			assertTrue(testLibrary.getClassMap().get("Computing and Methodology").get(i).equals(0));
		}
		
		// Since the library was normalized, values <5 are now 0...
		HashMap<String, Integer> map3 = new HashMap<String, Integer>();
		map3.put("Apples", 10);
		map3.put("Oranges", 0);
		map3.put("Lemons", 0);
		map3.put("Onions", 30);
		map3.put("Olives", 55);
		
		for(String i : allWords) { 
			if(map1.containsKey(i)) {
				assertTrue(testLibrary.getClassMap().get("General Literature").get(i).equals(map1.get(i)*5));
			}
			else if(map3.containsKey(i)) {
				assertTrue(testLibrary.getClassMap().get("General Literature").get(i).equals(map3.get(i)));
			}
			else {
				assertTrue(testLibrary.getClassMap().get("General Literature").get(i).equals(0));
			}
		}
		
		doc3.setClassification("Data");
		testLibrary = Trainer.train(doc3, testLibrary);
		classKeys = testLibrary.getClassMap().keySet();
		System.out.println("Testing : Train::train()");
		for(String i : classKeys) {
			wordKeys = testLibrary.getClassMap().get(i).keySet();
			String base = "--> | ";
			String buffer = " ";
			int numSpaces = (29 - i.length());
			for(int j=0; j<numSpaces; j++) {
				buffer = buffer + "-";
			}
			System.out.print(i + buffer + base);
			for(String k : wordKeys) {
				System.out.print(k + " " + testLibrary.getClassMap().get(i).get(k) + " | ");
			}
			System.out.println();
		}
		System.out.println();	
	}
//*** ------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_PruneDocSig() throws IOException {
		assertNotNull(testLibrary);
		assertTrue((testLibrary.getLibraryFile().toString()).equals(demoLib1.getCanonicalPath().toString()));
		assertTrue((testLibrary.getThrowOutWordsFile().toString()).equals(demoThrowOutWords2.getCanonicalPath().toString()));
		Set<String> keySet = map1.keySet();
		for(String i : keySet) {
			assertTrue(doc1.getDocSig().containsKey(i));
			assertTrue(doc2.getDocSig().containsKey(i));
			assertTrue(doc1.getDocSig().get(i).equals(map1.get(i)));
			assertTrue(doc2.getDocSig().get(i).equals(map1.get(i)));
		}
		Document prunedDoc = doc1;
		prunedDoc = Trainer.pruneDocSig(doc1, testLibrary);
		assertTrue(prunedDoc.getDocSig().equals(map2));
		prunedDoc = Trainer.pruneDocSig(doc2, testLibrary);
		assertTrue(prunedDoc.getDocSig().equals(map2));
		keySet = prunedDoc.getDocSig().keySet();
		/*
		for(String i : keySet) {
			System.out.println(i + " : " + prunedDoc.getDocSig().get(i));
		}*/
	}
//*** ------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_Finish() throws IOException {
		assertNotNull(testLibrary);
		
		doc3.setClassification("Computing Applications");
		testLibrary = Trainer.train(doc3, testLibrary);
		LinkedList<String> keepList = new LinkedList<String>();
		/*for(int i=0; i<testLibrary.getThrowOutWords().size(); i++) {
			wordList.add(testLibrary.getThrowOutWords().get(i));
		}*/
		keepList.add("anotherword1");
		keepList.add("anotherword2");
		
		// Need to save the state of files we we can revert after tests...
		LinkedList<String> wordList = new LinkedList<String>();
		HashMap<String, HashMap<String, Integer>> mapBefore = new HashMap<String, HashMap<String, Integer>>();
		LinkedList<String> wordsBefore = new LinkedList<String>();
		Scanner reader = new Scanner(demoLib1);
		String theWords = reader.nextLine();
		char[] charArr = theWords.toCharArray();
		String currWord = "";
		for(int i=0; i<charArr.length; i++) {
			if(charArr[i] == ' ') {
				wordList.add(currWord);
				currWord = "";
			}
			else {
				currWord = currWord + charArr[i];
			}
		}
		HashMap<String, Integer> value = new HashMap<String, Integer>();
		LinkedList<LinkedList<Integer>> listCeption = new LinkedList<LinkedList<Integer>>();
		String theLine;
		while(reader.hasNextLine()) {
			LinkedList<Integer> currList = new LinkedList<Integer>();
			theLine = reader.nextLine();
			char[] charA = theLine.toCharArray();
			String num = "";
			for(int i=0; i<charA.length; i++) {
				if(charA[i] == ' ') {
					Integer result = Integer.valueOf(num);
					currList.add(result);
					num = "";
				}
				else {
					num = num + charA[i];
				}
			}
			listCeption.add(currList);
		}
		reader.close();
		for(int i=0; i<listCeption.size(); i++) {
			String currKey = testLibrary.getClassifications()[i];
			value = new HashMap<String, Integer>();
			for(int k=0; k<wordList.size(); k++) {
				value.put(wordList.get(k), listCeption.get(i).get(k));
				mapBefore.put(currKey, value);
			}
		}
		wordsBefore.add("30");
		wordsBefore.add("thirty");
		
		HashMap<String, Integer> toCompare = new HashMap<String, Integer>();
		toCompare.put("sentence", 5);
		toCompare.put("normal", 5);
		toCompare.put("no", 5);
		toCompare.put("a", 10);
		toCompare.put("including", 5);
		toCompare.put("added", 5);
		toCompare.put("in", 10);
		toCompare.put("words", 5);
		toCompare.put("this", 10);
		toCompare.put("is", 10);
		toCompare.put("strange", 5);
		toCompare.put("the", 5);
		toCompare.put("characters", 5);
		toCompare.put("number", 5);
		toCompare.put("total", 5);
		toCompare.put("file", 5);
		toCompare.put("are", 10);
		toCompare.put("and", 5);
		toCompare.put("moderation", 5);
		toCompare.put("there", 10);
		toCompare.put("of", 5);
		toCompare.put("punctuation", 5);
		toCompare.put("30", 5);
		toCompare.put("thirty", 5);
		toCompare.put("Onions", 0);
		toCompare.put("Oranges", 0);
		toCompare.put("Apples", 0);
		toCompare.put("Lemons", 0);
		toCompare.put("Olives", 0);
		
		Trainer.finish(testLibrary, keepList);
		testLibrary.display();
		
		boolean fail = false;
		Set<String> theKeys = testLibrary.getClassMap().get("Computing Applications").keySet();
		for(String i : theKeys) {
			if(!(testLibrary.getClassMap().get("Computing Applications").containsKey(i))) {
				fail = true;
			}
		}
		keepList.clear();
		keepList.add("30");
		keepList.add("thirty");
		keepList.add("anotherword1");
		keepList.add("anotherword2");
		if(testLibrary.getThrowOutWords().size() != keepList.size()) {
			fail = true;
		}
		if(!(testLibrary.getThrowOutWords().equals(keepList))) {
			fail = true;
		}
		
		// Revert library.txt and throwOutWords.txt
		testLibrary.setClassMap(mapBefore);
		testLibrary.setThrowOutWords(wordsBefore);
		PrintWriter writer = new PrintWriter(testLibrary.getThrowOutWordsFile());
		writer.flush();
		for(int i=0; i<wordsBefore.size(); i++) {
			writer.println(wordsBefore.get(i));
		}
		writer.close();
		
		Trainer.finish(testLibrary, new LinkedList<String>());
		if(fail == true) {
			fail();
		}
		
		// Testing if pruning all words from library will leave library.txt in an invalid state...(It shouldn't)
		testLibrary = new Library();
		testLibrary.setLibraryFile(demoLib2);
		testLibrary.setThrowOutWordsFile(demoThrowOutWords2);
		testLibrary.readLibrary();
		testLibrary.readThrowOutWords();
		LinkedList<String> putBack = new LinkedList<String>();
		LinkedList<String> theReset = new LinkedList<String>();
		putBack.add("Apples");
		theReset.add("a");
		for(int i=0; i<11; i++) {
			putBack.add("1");
			theReset.add("0");
		}

		for(int i=0; i<testLibrary.getClassifications().length; i++) {
			doc3.setClassification(testLibrary.getClassifications()[i]);
			testLibrary = Trainer.train(doc3, testLibrary);
		}
		Trainer.finish(testLibrary, new LinkedList<String>());
		LinkedList<String> inFile = new LinkedList<String>();
		Scanner scanner = new Scanner(testLibrary.getLibraryFile());
		while(scanner.hasNext()) {
			inFile.add(scanner.next());
		}
		scanner.close();
		// Revert our test file to original...
		writer = new PrintWriter(demoLib2);
		for(int i=0; i<putBack.size(); i++) {
			writer.println(putBack.get(i) + " ");
		}
		writer.close();
		assertTrue(putBack.size() == 12);
		assertTrue(theReset.size() == 12);
		assertTrue(putBack.size() == inFile.size());
		assertTrue(theReset.size() == inFile.size());
		System.out.println(theReset);
		System.out.println(inFile);
		assertTrue(theReset.equals(inFile));
	}
//*** ------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_PopulateMap() {
		HashMap<String, String> toTest = new HashMap<String, String>();
		LinkedList<String> acmOptions = new LinkedList<String>();
		LinkedList<String> letterOpts = new LinkedList<String>();
		for(int i=0; i<testLibrary.getClassifications().length; i++) {
			acmOptions.add(testLibrary.getClassifications()[i]);
		}
		letterOpts.add("a");
		letterOpts.add("b");
		letterOpts.add("c");
		letterOpts.add("d");
		letterOpts.add("e");
		letterOpts.add("f");
		letterOpts.add("g");
		letterOpts.add("h");
		letterOpts.add("i");
		letterOpts.add("j");
		letterOpts.add("k");
		
		toTest = Trainer.populateMap();
		assertTrue(acmOptions.size() == letterOpts.size());
		assertTrue(toTest.size() == acmOptions.size());
		
		for(int i=0; i<letterOpts.size(); i++) {
			assertTrue(toTest.containsKey(letterOpts.get(i)));
		}
		for(int i=0; i<acmOptions.size(); i++) {
			assertTrue(toTest.containsValue(acmOptions.get(i)));
		}
	}
//*** ------------------------------------------------------------------------------------------------------------------- ***//
	
}  // End of TestTrainer.java