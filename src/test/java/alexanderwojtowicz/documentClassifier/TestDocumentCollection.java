package alexanderwojtowicz.documentClassifier;

import java.io.File;
import java.util.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestDocumentCollection {
//*** ------------------------------------------------------------------------------------------------------------------- ***//
	//** Data Member Declarations. **//
	File p1;        // Path to test1.txt
	File p2;        // Path to test4.pdf
	File p3;        // Path to test5.txt
	File p4;        // Path to 30words.pdf
	Document doc1;  // Valid Document #1 
	Document doc2;  // Valid Document #2 
	Document doc3;  // Valid Document #3
	Document doc4;  // Valid Document #4
	LinkedList<Document> docs;       // Put doc1, doc2, doc3 in this data structure.
	LinkedList<Document> emptyList;  // An empty linked list of documents.
	DocumentCollection col1;         // Uses Default Constructor.
	DocumentCollection col2;         // Construct collection with documents.
	DocumentCollection emptyCol;     // An empty document collection.
	String testDir;                  // Directory where test files are located.
	
	@Before
	public void SetUp() throws Exception {
		//** Data Member Initializations. **//
		// FROM: https://stackoverflow.com/questions/11747833/getting-filesystem-path-of-class-being-executed
		String classLoc = Document.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
		//** **//
		String tests = "/../../../../src/test/data/";
		
		testDir = new File(classLoc + tests).toString();
		p1      = new File(testDir + "/test1.txt");
		p2      = new File(testDir + "/test4.pdf");
		p3      = new File(testDir + "/test5.txt");
		p4      = new File(testDir + "/30words.pdf");
		doc1    = new Document(p1);
		doc2    = new Document(p2);
		doc3    = new Document(p3);
		doc4    = new Document(p4);
		
		emptyList = new LinkedList<Document>();
		docs = new LinkedList<Document>();
		docs.add(doc1);
		docs.add(doc2);
		docs.add(doc3);
		
		col1 = new DocumentCollection();
		col2 = new DocumentCollection(docs);
		emptyCol = new DocumentCollection();
	}
	
//*** --------------------------------------------------- TEST CASES ---------------------------------------------------- ***//
	@Test
	public void Test_DefaultConstructor() {
		assertNotNull(col1);
		assertThat(col1.getDocumentList(), is (emptyList));
		assertThat(col1.getSize(), is (emptyList.size()));
	}
//*** ------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_AlternateConstructor() {
		assertNotNull(col2);
		assertThat(col2.getDocumentList(), is (docs));
		assertThat(col2.getSize(), is (docs.size()));
	}
//*** ------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_SetDocumentList() {
		assertThat(col1.getDocumentList(), is (emptyList));
		col1.setDocumentList(docs);
		assertThat(col1.getDocumentList(), is (docs));
		docs.add(doc4);
		col1.setDocumentList(docs);
		assertThat(col1.getDocumentList(), is (docs));
	}
//*** ------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_AddToCollection() {
		assertThat(col1.getDocumentList(), is (emptyList));
		col1.addToCollection(doc1);
		emptyList.add(doc1);
		assertThat(col1.getDocumentList(), is (emptyList));
		col1.addToCollection(doc3);
		col1.addToCollection(doc4);
		emptyList.add(doc3);
		emptyList.add(doc4);
		assertThat(col1.getDocumentList(), is (emptyList));
	}
//*** ------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_Display() {
		assertThat(col2.getDocumentList(), is (docs));
		assertThat(col1.getDocumentList(), is (emptyList));
		col2.display();
		col1.display();
		
		// We don't want any side effects.
		assertThat(col2.getDocumentList(), is (docs));
		assertThat(col1.getDocumentList(), is (emptyList));
	}
//*** ------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_PrintClassifications() {
		assertThat(col2.getDocumentList(), is (docs));
		assertThat(col1.getDocumentList(), is (emptyList));
		col2.printClassifications();
		col1.printClassifications();
		
		// We don't want any side effects.
		assertThat(col2.getDocumentList(), is (docs));
		assertThat(col1.getDocumentList(), is (emptyList));
	}
//*** ------------------------------------------------------------------------------------------------------------------- ***//
} // End of TestDocumentCollection.java