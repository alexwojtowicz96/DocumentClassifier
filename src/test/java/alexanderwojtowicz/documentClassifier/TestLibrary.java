package alexanderwojtowicz.documentClassifier;

import static org.junit.Assert.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.BufferedReader;
import java.io.File;
//import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;
import java.util.Map.Entry;
import org.junit.Test;
import junit.framework.Assert;
import org.apache.cxf.helpers.FileUtils;
import org.junit.Before;
import static org.hamcrest.CoreMatchers.*;

public class TestLibrary {
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	Library testLibrary;
	HashMap<String, HashMap<String, Integer>> testCase1;
	HashMap<String, Integer> innerTestCase1;
	HashMap<String, HashMap<String, Integer>> libraryBefore;   // The instance of the library.txt file before library creation.
	
	String projDir;    // Directory: **/Blue1/
	File demoLib1;     // A sample library.
	File demoLib2;     // A sample library.
	File demoLib3;     // A sample library.
	
	String[] classifications = new String[] {"General Literature","Hardware",
			"Computer Systems Organization", "Software","Data",
			"Theory of Computation","Mathematics of Computing", 
			"Information Systems", "Computing and Methodology", 
			"Computing Applications","Computing Milieux"};
	
	@Before
	public void SetUp() throws IOException {
		String classLoc = TestLibrary.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
		String proj  = "/../../../../";
		projDir = (classLoc + proj);  // If using this in new file, use .getCanonicalFile()
		
		// Find out what the library in demoLib1.txt is before we create the new library object.
		String[] classifications = new String[] {"General Literature","Hardware",
				"Computer Systems Organization", "Software","Data",
				"Theory of Computation","Mathematics of Computing", 
				"Information Systems", "Computing and Methodology", 
				"Computing Applications","Computing Milieux"};
		
		libraryBefore = new HashMap<String, HashMap<String, Integer>>();
		demoLib1 = new File(projDir + "/src/test/data/demoLib1.txt");
		demoLib2 = new File(projDir + "/src/test/data/demoLib2.txt");
		demoLib3 = new File(projDir + "/src/test/data/demoLib3.txt");
		
		LinkedList<String> wordList = new LinkedList<String>();
		Scanner reader = new Scanner(demoLib1);
		String theWords = reader.nextLine();
		char[] charArr = theWords.toCharArray();
		String currWord = "";
		for(int i=0; i<charArr.length; i++) {
			if(charArr[i] == ' ') {
				wordList.add(currWord);
				currWord = "";
			}
			else {
				currWord = currWord + charArr[i];
			}
		}
		HashMap<String, Integer> value = new HashMap<String, Integer>();
		LinkedList<LinkedList<Integer>> listCeption = new LinkedList<LinkedList<Integer>>();
		String theLine;
		while(reader.hasNextLine()) {
			LinkedList<Integer> currList = new LinkedList<Integer>();
			theLine = reader.nextLine();
			char[] charA = theLine.toCharArray();
			String num = "";
			for(int i=0; i<charA.length; i++) {
				if(charA[i] == ' ') {
					Integer result = Integer.valueOf(num);
					currList.add(result);
					num = "";
				}
				else {
					num = num + charA[i];
				}
			}
			listCeption.add(currList);
		}
		reader.close();
		for(int i=0; i<listCeption.size(); i++) {
			String currKey = classifications[i];
			value = new HashMap<String, Integer>();
			for(int k=0; k<wordList.size(); k++) {
				
				value.put(wordList.get(k), listCeption.get(i).get(k));
				//System.out.print(wordList.get(k) + ":" + listCeption.get(i).get(k) + " ");
				libraryBefore.put(currKey, value);
			}
			//System.out.println();
		}
		/*
		Set<String> classKeys = libraryBefore.keySet();
		Set<String> wordKeys;
		for(String i : classKeys) {
			wordKeys = libraryBefore.get(i).keySet();
			System.out.print(i + " --> | ");
			for(String k : wordKeys) {
				System.out.print(k + " " + libraryBefore.get(i).get(k) + " | ");
			}
			System.out.println();
		}
		System.out.println();
		*/
		//************************************************************************************//
		testLibrary    = new Library();
		testCase1      = new HashMap<String, HashMap<String, Integer>>();
		innerTestCase1 = new HashMap<String, Integer>();
		innerTestCase1.put("Apples", 1);
		testCase1.put("General Literature", innerTestCase1);
		testCase1.put("Hardware", innerTestCase1);
		testCase1.put("Computer Systems Organization", innerTestCase1);
		testCase1.put("Software", innerTestCase1);
		testCase1.put("Data", innerTestCase1);
		testCase1.put("Theory of Computation", innerTestCase1);
		testCase1.put("Mathematics of Computing", innerTestCase1);
		testCase1.put("Information Systems", innerTestCase1);
		testCase1.put("Computing and Methodology", innerTestCase1);
		testCase1.put("Computing Applications", innerTestCase1);
		testCase1.put("Computing Milieux", innerTestCase1);
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@SuppressWarnings("deprecation")
	@Test
	public void Test_Constructor() throws Exception {
		assertNotNull(testLibrary);
		System.out.println(testLibrary.getThrowOutWordsFile().toString());
		assertTrue(testLibrary.getThrowOutWordsFile().exists());
		System.out.println(testLibrary.getLibraryFile().toString());
		assertTrue(testLibrary.getLibraryFile().exists());
		if(!testLibrary.getLibraryFile().exists()) {
			System.out.println("ERROR in 'Test_Constructor()': LibraryFile does not exist!");
			fail();
		}
		BufferedReader br = new BufferedReader(new FileReader(testLibrary.getLibraryFile()));
		if(br.readLine() == null) {
			assertTrue(testLibrary.getClassMap().equals(new HashMap<String, HashMap<String, Integer>>()));
		}
		else {
			assertFalse(testLibrary.getClassMap().equals(new HashMap<String, HashMap<String, Integer>>()));
		}
		br.close();
		
		LinkedList<String> toCompare = new LinkedList<String>();
		Scanner reader = new Scanner(testLibrary.getThrowOutWordsFile());
		while(reader.hasNext()) {
			String currWord = reader.next().toLowerCase();
			toCompare.add(currWord);
		}
		reader.close();
		assertTrue(toCompare.equals(testLibrary.getThrowOutWords()));
		Assert.assertEquals(FileUtils.readLines(testLibrary.getLibraryFile()),
				FileUtils.readLines(new File(projDir + "/src/main/resources/library.txt").getCanonicalFile()));	
		
		testLibrary.setLibraryFile(demoLib1);
		testLibrary.readLibrary();  // classMap should be updated to libraryBefore
		// If we run into reading issues, add more checks for every key : value pair in the sub hash maps of the big hash map.
		assertThat(testLibrary.getClassMap().size(), is (libraryBefore.size()));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_Prune() throws IOException {
		HashMap<String, HashMap<String, Integer>> testPrune = new HashMap<String, HashMap<String, Integer>>();
		testPrune = testLibrary.prune(testCase1);
		Iterator<Entry<String,HashMap<String,Integer>>> testIt = testPrune.entrySet().iterator();
		while(testIt.hasNext()) {
			Entry<String,HashMap<String,Integer>> findError = testIt.next();
			assertTrue(findError.getValue().isEmpty());
		}
		
		testLibrary.setLibraryFile(demoLib3);
		testLibrary.readLibrary();
		testPrune = testLibrary.prune(testLibrary.getClassMap());
		testLibrary.setClassMap(testPrune);
		
		Set<String> classKeys = testLibrary.getClassMap().keySet();
		Set<String> wordKeys;
		System.out.println("\nTesting : Library::prune()");
		for(String i : classKeys) {
			wordKeys = testLibrary.getClassMap().get(i).keySet();
			String base = "--> | ";
			String buffer = " ";
			int numSpaces = (29 - i.length());
			for(int j=0; j<numSpaces; j++) {
				buffer = buffer + "-";
			}
			System.out.print(i + buffer + base);
			for(String k : wordKeys) {
				System.out.print(k + " " + testLibrary.getClassMap().get(i).get(k) + " | ");
			}
			System.out.println();
		}
		System.out.println();
		
		assertTrue(testLibrary.getClassMap().size() == 11);
		for(String c : classKeys) {
			assertTrue(testLibrary.getClassMap().get(c).size() == 2);
			assertFalse(testLibrary.getClassMap().get(c).containsKey("cucumber"));
			assertTrue(testLibrary.getClassMap().get(c).containsKey("apple"));
			assertTrue(testLibrary.getClassMap().get(c).containsKey("banana"));
		}
	}	
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_FindPrune() throws IOException {		
		List<String> testFindPrune = new Vector<String>();
		testFindPrune = testLibrary.findPrune(testCase1);
		assertTrue(testFindPrune.contains("Apples"));
		
		testLibrary.setLibraryFile(demoLib3);
		testLibrary.readLibrary();
		testFindPrune = testLibrary.findPrune(testLibrary.getClassMap());
		System.out.println("***********************************************");
		System.out.println("toPrune -> " + testFindPrune);
		System.out.println("***********************************************");
		assertThat(testFindPrune.size(), is (1));
		assertTrue(testFindPrune.contains("cucumber"));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_ReadLibrary() throws IOException {
		String fileLoc = projDir + "/src/test/data/";
		File file = new File(fileLoc + "ReadLibraryTest.txt");
		testLibrary.setLibraryFile(file);
		testLibrary.readLibrary();
		HashMap<String, HashMap<String,Integer>> classMap = testLibrary.getClassMap();
		Set<String> words = classMap.get("General Literature").keySet();
		boolean pass = true;
		for(String i: classifications) {
			for(String j: words) {
				if(classMap.get(i).get(j) != testCase1.get(i).get(j)) {
					pass = false;
				}
			}
		}
		assertTrue(pass);
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_WriteLibrary() throws IOException {
		String fileLoc = projDir + "/src/test/data/";
		File writefile = new File(fileLoc + "WriteLibraryTest.txt");
		File readfile = new File(fileLoc + "ReadLibraryTest.txt");
		testLibrary.setClassMap(testCase1);
		testLibrary.setLibraryFile(writefile);
		testLibrary.writeLibrary();
		Scanner swrite = new Scanner(writefile);
		Scanner sread = new Scanner(readfile);
		String write = new String();
		String read = new String();
		while(swrite.hasNextLine()) {
			write = write + swrite.nextLine();
		}
		while(sread.hasNextLine()) {
			read = read + sread.nextLine();
		}
		assertEquals(write, read);
		swrite.close();
		sread.close();
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_AddToThrowOutWordsFile() throws IOException {
		assertNotNull(testLibrary);
		System.out.println(testLibrary.getThrowOutWordsFile().toString());
		assertTrue(testLibrary.getThrowOutWordsFile().exists());
		
		LinkedList<String> words = new LinkedList<String>();
		Scanner reader = new Scanner(new File(testLibrary.getThrowOutWordsFile().toString()));
		while(reader.hasNext()) {
			words.add(reader.next());
		}
		reader.close();
		
		List<String> toAdd = new Vector<String>();
		toAdd.add("thisaintaword");
		toAdd.add("hereisanotherword");
		toAdd.add("40jkler");
		
		testLibrary.addToThrowOutWordsFile(toAdd);
		LinkedList<String> wordsAfter = new LinkedList<String>();
		reader = new Scanner(new File(testLibrary.getThrowOutWordsFile().toString()));
		while(reader.hasNext()) {
			wordsAfter.add(reader.next());
		}
		reader.close();
		
		// Revert the file back to its original state
		PrintWriter writer = new PrintWriter(testLibrary.getThrowOutWordsFile());
		for(int i=0; i<words.size(); i++) {
			writer.println(words.get(i));
		}
		writer.close();
		
		assertThat(wordsAfter.size(), is (words.size() + 3));
		for(int i=0; i<words.size(); i++) {
			assertTrue(wordsAfter.contains(words.get(i)));
		}
		assertTrue(wordsAfter.contains("thisaintaword"));
		assertTrue(wordsAfter.contains("hereisanotherword"));
		assertTrue(wordsAfter.contains("40jkler"));	
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@SuppressWarnings("deprecation")
	@Test
	public void Test_SetLibraryFile() throws IOException, Exception {
		assertNotNull(testLibrary);
		assertTrue(testLibrary.getLibraryFile().exists());
		Assert.assertEquals(FileUtils.readLines(testLibrary.getLibraryFile()),
				FileUtils.readLines(new File(projDir + "/src/main/resources/library.txt").getCanonicalFile()));	
		
		testLibrary.setLibraryFile(demoLib1);
		Assert.assertEquals(FileUtils.readLines(testLibrary.getLibraryFile()),
				FileUtils.readLines(new File(demoLib1.toString()).getCanonicalFile()));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@SuppressWarnings("deprecation")
	@Test
	public void Test_SetThrowOutWordsFile() throws Exception {
		assertNotNull(testLibrary);
		if(!testLibrary.getThrowOutWordsFile().exists()) {
			System.out.println("ERROR: testLibrary.getThrowOutWordsFile().exists() == False!");
			fail();
		}
		else {
			System.out.println("Initial throwOutWordsFile was found in : " + testLibrary.getThrowOutWordsFile().toString());
		}
		Assert.assertEquals(FileUtils.readLines(testLibrary.getThrowOutWordsFile()),
				FileUtils.readLines(new File(projDir + "/src/main/resources/throwOutWords.txt").getCanonicalFile()));	
		
		File demoThrowOutWords1 = new File(projDir + "/src/test/data/demoThrowOutWords1.txt");
		testLibrary.setThrowOutWordsFile(demoThrowOutWords1);
		if(!testLibrary.getThrowOutWordsFile().exists()) {
			System.out.println("Test_SetThrowOutWordsFile() FAILED: Cannot locate -> " + demoThrowOutWords1.toString());
			fail();
		}
		else {
			System.out.println("Test_SetThrowOutWordsFile(): File Found at -> " + testLibrary.getThrowOutWordsFile().toString());
			Assert.assertEquals(FileUtils.readLines(testLibrary.getThrowOutWordsFile()),
					FileUtils.readLines(new File(demoThrowOutWords1.toString()).getCanonicalFile()));
		}
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_SetThrowOutWords() throws IOException {
		assertNotNull(testLibrary);
		BufferedReader br = new BufferedReader(new FileReader(testLibrary.getThrowOutWordsFile()));
		if(br.readLine() == null) {
			assertTrue(testLibrary.getThrowOutWords().equals(new LinkedList<String>()));
		}
		else {
			assertFalse(testLibrary.getThrowOutWords().equals(new LinkedList<String>()));
		}
		br.close();
		LinkedList<String> newList = new LinkedList<String>();
		newList.add("word1");
		newList.add("word2");
		newList.add("word3");
		testLibrary.setThrowOutWords(newList);
		assertTrue(testLibrary.getThrowOutWords().equals(newList));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_SetClassMap() throws IOException {
		assertNotNull(testLibrary);
		BufferedReader br = new BufferedReader(new FileReader(testLibrary.getLibraryFile()));
		if(br.readLine() == null) {
			assertTrue(testLibrary.getClassMap().equals(new HashMap<String, HashMap<String, Integer>>()));
		}
		else {
			assertFalse(testLibrary.getClassMap().equals(new HashMap<String, HashMap<String, Integer>>()));
		}
		br.close();
		
		testLibrary.setClassMap(libraryBefore);
		assertFalse(testLibrary.getClassMap().equals(new HashMap<String, HashMap<String, Integer>>()));
		assertThat(testLibrary.getClassMap(), is (libraryBefore));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_ReadThrowOutWords() throws IOException {
		assertNotNull(testLibrary);
		LinkedList<String> oldList = testLibrary.getThrowOutWords();
		LinkedList<String> expected = new LinkedList<String>();
		expected.add("30");
		expected.add("thirty");
		
		File demoThrowOutWords2 = new File(projDir + "/src/test/data/demoThrowOutWords2.txt").getCanonicalFile();
		testLibrary.setThrowOutWordsFile(demoThrowOutWords2);
		testLibrary.readThrowOutWords();
		assertTrue(!oldList.equals(expected));
		assertTrue(!oldList.equals(testLibrary.getThrowOutWords()));
		assertTrue(testLibrary.getThrowOutWords().equals(expected));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_Display() throws IOException {
		assertNotNull(testLibrary);
		testLibrary.setLibraryFile(demoLib1);
		testLibrary.setThrowOutWordsFile(new File(projDir + "/src/test/data/demoThrowOutWords1.txt").getCanonicalFile());
		testLibrary.readLibrary();
		testLibrary.readThrowOutWords();
		
		HashMap<String, HashMap<String, Integer>> mapBefore = testLibrary.getClassMap();
		LinkedList<String> wordsBefore = testLibrary.getThrowOutWords();
		File libFileBefore = testLibrary.getLibraryFile();
		File wordsFileBefore = testLibrary.getThrowOutWordsFile();
		
		// We don't want side effects
		testLibrary.display();
		
		assertThat(testLibrary.getClassMap(), is (mapBefore));
		assertThat(testLibrary.getThrowOutWords(), is (wordsBefore));
		assertThat(testLibrary.getLibraryFile(), is (libFileBefore));
		assertThat(testLibrary.getThrowOutWordsFile(), is (wordsFileBefore));
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_Normalize() throws IOException {
		assertNotNull(testLibrary);
		System.out.println("\nTEST_NORMALIZE -> ***************************************************\n");
		testLibrary.setLibraryFile(demoLib2);
		testLibrary.setThrowOutWordsFile(new File(projDir + "/src/test/data/demoThrowOutWords1.txt").getCanonicalFile());
		testLibrary.readLibrary();
		testLibrary.readThrowOutWords();
		testLibrary.display();
		testLibrary.normalize();
		System.out.println("The following is the normalized library...");
		testLibrary.display();
		
		// This will pass if "apple", "banana", and "cucumber" are removed from classMap.
		LinkedList<String> discarded = new LinkedList<String>();
		HashMap<String, Integer> tempMap = new HashMap<String, Integer>();
		discarded.add("apple");
		discarded.add("banana");
		discarded.add("cucumber");
		
		// A) GENERAL LITERATURE
		tempMap.put("daikon", 5); tempMap.put("egg", 5); tempMap.put("fruit", 6);
		tempMap.put("grape", 7); tempMap.put("holly", 8); tempMap.put("igloo", 19);
		tempMap.put("jelly", 5); tempMap.put("kelvin", 5);
		Set<String> words = tempMap.keySet();
		assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[0]).size() == tempMap.size());
		for(String i : words) {
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[0]).containsKey(i));
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[0]).get(i) == tempMap.get(i));
		}
		for(int i=0; i<discarded.size(); i++) {
			assertFalse(testLibrary.getClassMap().get(testLibrary.getClassifications()[0]).containsKey(discarded.get(i)));
		}
		tempMap.clear();
		
		// B) HARDWARE
		tempMap.put("daikon", 5); tempMap.put("egg", 5); tempMap.put("fruit", 5);
		tempMap.put("grape", 5); tempMap.put("holly", 5); tempMap.put("igloo", 11);
		tempMap.put("jelly", 7); tempMap.put("kelvin", 7);
		assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[1]).size() == tempMap.size());
		for(String i : words) {
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[1]).containsKey(i));
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[1]).get(i) == tempMap.get(i));
		}
		for(int i=0; i<discarded.size(); i++) {
			assertFalse(testLibrary.getClassMap().get(testLibrary.getClassifications()[1]).containsKey(discarded.get(i)));
		}
		tempMap.clear();
		
		// C) COMPUTER SYSTEMS ORGANIZATION
		tempMap.put("daikon", 7); tempMap.put("egg", 7); tempMap.put("fruit", 7);
		tempMap.put("grape", 7); tempMap.put("holly", 7); tempMap.put("igloo", 7);
		tempMap.put("jelly", 7); tempMap.put("kelvin", 7);
		assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[2]).size() == tempMap.size());
		for(String i : words) {
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[2]).containsKey(i));
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[2]).get(i) == tempMap.get(i));
		}
		for(int i=0; i<discarded.size(); i++) {
			assertFalse(testLibrary.getClassMap().get(testLibrary.getClassifications()[2]).containsKey(discarded.get(i)));
		}
		tempMap.clear();
		
		// D) SOFTWARE
		tempMap.put("daikon", 0); tempMap.put("egg", 0); tempMap.put("fruit", 0);
		tempMap.put("grape", 0); tempMap.put("holly", 0); tempMap.put("igloo", 0);
		tempMap.put("jelly", 0); tempMap.put("kelvin", 0);
		assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[3]).size() == tempMap.size());
		for(String i : words) {
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[3]).containsKey(i));
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[3]).get(i) == tempMap.get(i));
		}
		for(int i=0; i<discarded.size(); i++) {
			assertFalse(testLibrary.getClassMap().get(testLibrary.getClassifications()[3]).containsKey(discarded.get(i)));
		}
		tempMap.clear();
		
		// E) DATA
		tempMap.put("daikon", 0); tempMap.put("egg", 0); tempMap.put("fruit", 0);
		tempMap.put("grape", 0); tempMap.put("holly", 0); tempMap.put("igloo", 0);
		tempMap.put("jelly", 0); tempMap.put("kelvin", 0);
		assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[4]).size() == tempMap.size());
		for(String i : words) {
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[4]).containsKey(i));
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[4]).get(i) == tempMap.get(i));
		}
		for(int i=0; i<discarded.size(); i++) {
			assertFalse(testLibrary.getClassMap().get(testLibrary.getClassifications()[4]).containsKey(discarded.get(i)));
		}
		tempMap.clear();
		
		// F) THEORY OF COMPUTATION
		tempMap.put("daikon", 100); tempMap.put("egg", 100); tempMap.put("fruit", 100);
		tempMap.put("grape", 100); tempMap.put("holly", 100); tempMap.put("igloo", 100);
		tempMap.put("jelly", 100); tempMap.put("kelvin", 100);
		assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[5]).size() == tempMap.size());
		for(String i : words) {
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[5]).containsKey(i));
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[5]).get(i) == tempMap.get(i));
		}
		for(int i=0; i<discarded.size(); i++) {
			assertFalse(testLibrary.getClassMap().get(testLibrary.getClassifications()[5]).containsKey(discarded.get(i)));
		}
		tempMap.clear();
		
		// G) MATHEMATICS OF COMPUTING
		tempMap.put("daikon", 0); tempMap.put("egg", 0); tempMap.put("fruit", 0);
		tempMap.put("grape", 0); tempMap.put("holly", 0); tempMap.put("igloo", 0);
		tempMap.put("jelly", 0); tempMap.put("kelvin", 80);
		assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[6]).size() == tempMap.size());
		for(String i : words) {
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[6]).containsKey(i));
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[6]).get(i) == tempMap.get(i));
		}
		for(int i=0; i<discarded.size(); i++) {
			assertFalse(testLibrary.getClassMap().get(testLibrary.getClassifications()[6]).containsKey(discarded.get(i)));
		}
		tempMap.clear();
		
		// H) INFORMATION SYSTEMS
		tempMap.put("daikon", 0); tempMap.put("egg", 0); tempMap.put("fruit", 0);
		tempMap.put("grape", 0); tempMap.put("holly", 0); tempMap.put("igloo", 0);
		tempMap.put("jelly", 0); tempMap.put("kelvin", 0);
		assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[7]).size() == tempMap.size());
		for(String i : words) {
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[7]).containsKey(i));
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[7]).get(i) == tempMap.get(i));
		}
		for(int i=0; i<discarded.size(); i++) {
			assertFalse(testLibrary.getClassMap().get(testLibrary.getClassifications()[7]).containsKey(discarded.get(i)));
		}
		tempMap.clear();
		
		// I) COMPUTING AND METHODOLOGY
		tempMap.put("daikon", 0); tempMap.put("egg", 0); tempMap.put("fruit", 0);
		tempMap.put("grape", 0); tempMap.put("holly", 0); tempMap.put("igloo", 0);
		tempMap.put("jelly", 0); tempMap.put("kelvin", 5);
		assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[8]).size() == tempMap.size());
		for(String i : words) {
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[8]).containsKey(i));
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[8]).get(i) == tempMap.get(i));
		}
		for(int i=0; i<discarded.size(); i++) {
			assertFalse(testLibrary.getClassMap().get(testLibrary.getClassifications()[8]).containsKey(discarded.get(i)));
		}
		tempMap.clear();
		
		// J) COMPUTING APPLICATIONS
		tempMap.put("daikon", 0); tempMap.put("egg", 0); tempMap.put("fruit", 0);
		tempMap.put("grape", 0); tempMap.put("holly", 0); tempMap.put("igloo", 0);
		tempMap.put("jelly", 0); tempMap.put("kelvin", 0);
		assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[9]).size() == tempMap.size());
		for(String i : words) {
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[9]).containsKey(i));
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[9]).get(i) == tempMap.get(i));
		}
		for(int i=0; i<discarded.size(); i++) {
			assertFalse(testLibrary.getClassMap().get(testLibrary.getClassifications()[9]).containsKey(discarded.get(i)));
		}
		tempMap.clear();
		
		// K) COMPUTING MILIEUX
		tempMap.put("daikon", 0); tempMap.put("egg", 0); tempMap.put("fruit", 0);
		tempMap.put("grape", 0); tempMap.put("holly", 0); tempMap.put("igloo", 0);
		tempMap.put("jelly", 0); tempMap.put("kelvin", 0);
		assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[10]).size() == tempMap.size());
		for(String i : words) {
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[10]).containsKey(i));
			assertTrue(testLibrary.getClassMap().get(testLibrary.getClassifications()[10]).get(i) == tempMap.get(i));
		}
		for(int i=0; i<discarded.size(); i++) {
			assertFalse(testLibrary.getClassMap().get(testLibrary.getClassifications()[10]).containsKey(discarded.get(i)));
		}
		tempMap.clear();
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_GenerateCsv() throws IOException {
		testLibrary = new Library();
		assertNotNull(testLibrary);
		testLibrary.generateCsv();
    	final String classLoc = Library.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
    	final String dir = "../../../../src/main/resources/";
    	final String fileDir = (new File(classLoc + dir).getCanonicalFile()).toString();
    	final String ext = ".csv";
    	
    	File theFile = new File(fileDir + "/library" + ext);
    	assertTrue(theFile.exists());
    	testLibrary.deleteGeneratedCsv();
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//
	@Test
	public void Test_DeleteGeneratedCsv() throws IOException {
		testLibrary = new Library();
		assertNotNull(testLibrary);
    	final String classLoc = Library.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
    	final String dir = "../../../../src/main/resources/";
    	final String fileDir = (new File(classLoc + dir).getCanonicalFile()).toString();
    	final String ext = ".csv";
    	File theFile = new File(fileDir + "/library" + ext);
    	
    	testLibrary.generateCsv();
    	assertTrue(theFile.exists());
    	testLibrary.deleteGeneratedCsv();
    	assertFalse(theFile.exists());   	
	}
//*** -------------------------------------------------------------------------------------------------------------------- ***//

	@Test
	public void Test_generateArff() throws IOException {
		testLibrary = new Library();
		assertNotNull(testLibrary);
		String fileLoc = projDir + "/src/test/data/";
		testLibrary.setClassMap(testCase1);
		testLibrary.setLibraryFile(demoLib2);
		testLibrary.readLibrary();
		testLibrary.generateArff();
		File testfile = new File(fileLoc + "testArff.arff");
		final String classLoc = Library.class.getProtectionDomain().getCodeSource().getLocation().getPath().toString();
    	final String dir = "../../../../src/main/resources/";
    	final String fileDir = (new File(classLoc + dir).getCanonicalFile()).toString();
    	File outputFile = new File(fileDir + "/library" + ".arff");
    	Scanner test = new Scanner(testfile);
		Scanner output = new Scanner(outputFile);
		String testString = new String();
		String outputString = new String();
		while(test.hasNextLine())
		{
			testString = testString + test.nextLine();
		}
		while(output.hasNextLine())
		{
			outputString = outputString + output.nextLine();
		}
		assertEquals(testString, outputString);
		test.close();
		output.close();
	}
} // End of TestLibrary.java